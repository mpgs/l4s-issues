% !TeX root = ecn-fallback_tr.tex
% ================================================================
\section{Implementation in Integer Arithmetic}\label{ecn-fallbacktr_impl}

% ----------------------------------------------------------------
\subsection{EWMA Precision and Upscaling}\label{ecn-fallbacktr_precision}

The pseudocode below repeats the EWMA pseudocode given in
\autoref{ecn-fallbacktr_pseudocode_srtt}, but in integer arithmetic including recommended
types for the variables and the formulae used to check for overflow.
Rationale for the recommended types is given below the pseudocode.

All the variables that are specific to Classic ECN AQM fall-back are
prefixed with \texttt{fbk\_}, so the EWMAs of srtt and mdev are called
\texttt{fbk\_srtt} and \texttt{fbk\_mdev}.\footnote{To save space in the
TCP control block, it may be preferred to solely store g\_srtt\_shift and
recalculate g\_mdev\_shift as needed.}

\begin{verbatim}
#define ACC_MRTT_MAX 0xFFFFFFUL    // < 2^24 [us] (=16.7s)
#define FBK_SSTHRESH_MAX 0x0FFF    // < 2^((20 - 2) * 2/3) - 1 = 2^(12)  (see text)
#define FBK_G_DIFF 1               // fbk_g_mdev = fbk_g_srtt * 2^(FBK_G_DIFF)

// Stored variables
u64 fbk_srtt;                // Upscaled smoothed RTT
u64 fbk_mdev;                // Upscaled mean deviation
int fbk_g_srtt_shift;        // srtt gain bit-shift (initialized dependent on ssthresh)
int fbk_g_mdev_shift;        // mdev gain bit-shift (initialized dependent on ssthresh)
u32 fbk_mdev_carry;          // Geometric carry
u32 fbk_depth_carry;         // Geometric carry

// Temporary variables
u32 acc_mrtt;                // Measured RTT from newest unack'd packet
s64 error_;
int delta_shift_;

{   // At start of connection, either after first RTT measurement or from dst cache
    fbk_srtt = acc_mrtt<<fbk_g_srtt_shift;
    fbk_mdev = 1ULL<<fbk_g_mdev_shift; // No need for conservative init, unlike for RTO
}

{   // Per ACK
    acc_mrtt = min(acc_mrtt, ACC_MRTT_MAX);
    // Update EWMAs
    error_ = (u64)acc_mrtt - fbk_srtt>>fbk_g_srtt_shift;
    fbk_srtt += error_;
    fbk_mdev += llabs(error_) - fbk_mdev>>fbk_g_mdev_shift;    // See text re llabs()
}

{   // Per ssthresh change, including when initialized
    delta_shift_ = -fbk_g_srtt_shift;               // Store old shift
    fbk_g_srtt_shift = ilog2(min(ssthresh, FBK_SSTHRESH_SHIFT_MAX));
    fbk_g_srtt_shift += fbk_g_srtt_shift>>1 + 1;    // fbk_g_srtt = 2 * ssthresh^(3/2)
    fbk_g_mdev_shift = fbk_g_srtt_shift + FBK_G_DIFF;   // fbk_g_mdev = fbk_g_srtt * 2

    // Adjust all upscaled variables
    delta_shift_ += fbk_g_srtt_shift;     // Difference between old and new shift
    if (delta_shift_ > 0) {
        fbk_srtt <<= delta_shift_;
        fbk_srtt_carry <<= delta_shift_;
        fbk_mdev <<= delta_shift_;        // Same shift for mdev
        fbk_mdev_carry <<= delta_shift_;
    } else if (delta_shift != 0) {
        delta_shift_ = -delta_shift_;
        fbk_srtt >>= delta_shift_;
        fbk_srtt_carry >>= delta_shift_;
        fbk_mdev >>= delta_shift_;        // Same shift for mdev
        fbk_mdev_carry >>= delta_shift_;
    }
}
\end{verbatim}

The question of the size of the EWMA variables can be broken down into
the number range needed prior to upscaling, and the maximum upscaling to
be supported.

\paragraph{EWMA Range Prior to Upscaling:} Both the EWMAs
(\texttt{fbk\_srtt} and \texttt{fbk\_mdev}) are fed by the measured RTT
\texttt{acc\_mrtt} so (prior to upscaling) they never need to hold a
value greater than the max usable value of \texttt{acc\_mrtt}.

RFC793 defines the maximum segment lifetime (MSL) as 2 minutes (implying
a maximum RTT of 4 minutes = \(2^{28}\,\mu\)s), but it would do no harm
if the fall-back algorithm clamped any RTT exceeding, say 15\,s to a
maximum. A bloated buffer might well lead to an average RTT of a few
seconds, but outliers beyond 15\,s would unnecessarily pollute the
average. If most RTT measurements were this high, there would be little
benefit in making classic ECN detection so sluggish. Therefore it will be
reasonable to clamp \texttt{acc\_mrtt} below \(2^{24}\,\mu\)s (16.7\,s)
and use a 32-bit unsigned long integer for it.

\paragraph{Upscaling Limit:} In order to implement an EWMA in integer
arithmetic, upscaling the EWMA by the reciprocal \(g\) of its gain
optimizes the most frequently run parts of the code---those run per ACK.
Upscaling by \(g\) is straightforward when constant gain is used. But
when the gain is adapted (see \autoref{ecn-fallbacktr_srtt_adapt}) it is
necessary to change the upscaling of the EWMAs, and one or two other
variables that depend on this upscaling.

We tried the alternative of always upscaling by a constant maximum
factor.\footnote{Another alternative might be to use the same upscaling
factor for both \texttt{fbk\_srtt} and \texttt{fbk\_mdev} even if their
gains are different.} This does indeed simplify the calculations needed
per sawtooth,
but it adds 3 or 4 bit-shifts to the per-ACK calculations. On balance, it
was decided to prioritize optimization of per-ACK processing, given the
extra per-sawtooth cost is only about half-a-dozen simple operations (a
couple of adds; an if; and a few bit-shifts).\footnote{Incidentally,
while we are talking about the `Per ACK' block, it is particularly
important not to use inefficient code here. The
\texttt{llabs()} function is used to take the absolute value of
\texttt{error\_}. The implementation of \texttt{llabs()} is not shown,
but it is intended to be similar to the assembler used in the function of
the same name in stdlib.h, which returns the absolute value of a long
long signed integer.} This involves
upscaling by a variable amount that depends on the gain. However, it
still leaves the question of what upper bound to fix for upscaling.

A workable upper bound for the number of ACKs over which smoothing is
carried out would be \(2^{20}\) (a little over 2 minutes at 200\,Mb/s
assuming 1500\,B packets and a delayed ACK every 2 packets). As flow rate
scaled beyond this, the adaptation mechanism would stop growing the
number of ACKs over which smoothing would occur and the ACK rate would
increase, so the maximum smoothing time would reduce, although this might
be mitigated somewhat by an increase in the ACK ratio. However,
\(2^{20}\) is believed to cater for sufficient scale, for the three
reasons given in \autoref{ecn-fallbacktr_srtt_adapt} (sufficient for
transition out of an unscalable regime; the likelihood of disturbance
within this time; and ensuring that detection remains responsive).

In the `Per ssthresh change' block of the above pseudocode, when
\texttt{fbk\_g\_srtt\_shift} is derived from \texttt{ssthresh}, instead
of calculating the unbounded value then clamping it, the intermediate
value is clamped to an adjusted down limit before it is raised to the
power of 3/2 and doubled. This limit is held in the constant macro
\texttt{FBK\_SSTHRESH\_SHIFT\_MAX}, which is reverse engineered as
\((20-2)*2/3 = 12\). This does not limit ssthresh itself. It is just the
value of ssthresh at which to cap the number of ACKs over which smoothing
is carried out.

\paragraph{Precision and Upscaling Summary:} \texttt{acc\_mrtt} uses 23
significant bits, and \texttt{fbk\_g\_mdev\_shift} upscales by a maximum
of 19 bits, making 42 bits for the largest EWMA. Therefore the EWMAs will
easily fit in a u64, but it will not fit in a u32.

% ----------------------------------------------------------------
\subsection{Iterative Fast Log Calculations with Geometric Carrying}\label{ecn-fallbacktr_ilog}

The Classic ECN detection algorithm takes the ratios between the
\texttt{fbk\_srtt} or \texttt{fbk\_mdev} metrics and their reference
values and transforms them into linear step changes in the
\texttt{classic\_ecn} score. This requires a log function but, rather
than using floating point arithmetic, it uses the base 2 integer log
function \texttt{ilog2()}, which has very low processing cost, but also
very little precision. Essentially \texttt{ilog2()} returns the binary
order of magnitude of the operand, i.e.\ the position of the most
significant bit that is set to 1 (typically using an assembler
instruction such as clz, which stands for count leading zeros).

The order-of-magnitude precision of \texttt{ilog2()} is sometimes good
enough, but not always. It is sensitive to whether the configured
threshold value is close to a step change in the integer log, which can
cause behaviour to stick then suddenly toggle. For instance if the
threshold were set to \(\mathtt{V0}=500\,\mu\)s, in floating point
arithmetic \(\mathrm{lg}(500)=8.965784285\), but in integer arithmetic
\(\mathrm{ilog2}(500)=8\). So RTT values lower than the threshold, down
to \(257\,\mu\)s would appear to be at the threshold.

Rather than resort to floating point arithmetic, we implemented an
iterative carry algorithm, \texttt{carry\_ilog2()}, to accumulate the
error and feed it back into the log function, which then outputs the
integer values either side of the precise answer, in proportion to the
error term at any one time. The cost is 3 adds, 2 bit-shifts and 1
integer multiply. In following pseudocode It is defined at the end and
used twice in the `Per RTT' block.

\begin{verbatim}
#define PRAGUE_ALPHA_BITS	20U  // Upscaling of classic_ecn
#define V_LG 1                 // Weight of queue *V*ariability metric, default 2^(-1)
#define D_LG 1                 // Weight of mean queue *D*epth metric, default 2^(-1)
#define V0_LG_US (10014684UL >> V) // Ref queue *V*ariability [us], default lg(750)<<20
#define D0 2000                    // Ref queue *D*epth [us]
#define D0_LG_US (11498458UL >> D) // Ref queue *D*epth [us], default lg(2000)<<20

// Stored variables
int classic_ecn;               // Classic ECN AQM detection score
u32 rtt_min;                   // Min RTT (uses Kathleen Nichols's windowed min tracker)

{   // At start of connection, either after first RTT measurement or from dst cache
    fbk_mdev_carry = 1 << fbk_g_mdev_shift;
    fbk_mdev_carry += 1 << (fbk_g_mdev_shift - 1);       // Initialize to 3/2 upscaled
    fbk_depth_carry = 1 << fbk_g_srtt_shift;
    fbk_depth_carry += 1 << (fbk_g_srtt_shift - 1);      // Initialize to 3/2 upscaled
}

{   // Per RTT
    // Temporary variables for readability
    u64 fbk_mdev_;
    u64 fbk_depth_;
    int fbk_mdev_lg_;
    int fbk_depth_lg_;

    fbk_mdev_ = fbk_mdev>>fbk_g_mdev_shift;               // remove upscaling
    fbk_mdev_lg_ = carry_ilog2(fbk_mdev_, fbk_g_mdev_shift, *fbk_mdev_carry)
    // V*lg(v/V0)
    classic_ecn += (u64)fbk_mdev_lg_<<(PRAGUE_ALPHA_BITS - V);
    classic_ecn -=  V0_LG_US;

    fbk_depth_ = fbk_srtt>>fbk_g_srtt_shift - rtt_min;    // Smoothed q depth
    fbk_depth_lg_ = carry_ilog2(fbk_depth_, fbk_g_srtt_shift, *fbk_srtt_carry)
    // D*lg(max(d/D0,1))
    fbk_depth_lg_ <<= PRAGUE_ALPHA_BITS - D
    if (fbk_depth_lg_ > D0_LG_US) {
        classic_ecn += (u64)fbk_depth_lg_ - D0_LG_US;
    }
}

int carry_ilog2(u64 arg, int shift,  u32 *carry) {
    // returns integer base 2 log of arg factored up by carry upscaled by shift
    int arg_lg_;

    // Multiply non-upscaled arg by upscaled geometric carry from previous round
    arg *= *carry;
    arg += 1 << (shift - 1);  // Add upscaled 1/2 to unbias truncation
    arg_lg_ = ilog2(arg) - shift;         // Non-upscaled integer log
    *carry = arg >> arg_lg_;              // Upscaled geometric carry
    return arg_lg_;
}
\end{verbatim}

Being a log function, the carry algorithm needs to be multiplicative
(geometric), not additive. The comment `Upscaled geometric carry' tags
the line that calculates the carry factor, pointed to by \texttt{carry},
by right bit-shifting the upscaled value of \texttt{arg} by its own
integer log.

For instance, if the value of \texttt{fbk\_mdev} passed to the algorithm
is \(500\,\mu\)s, \(\mathrm{ilog2}(500)=8\), so bit-shifting 500 by 8 is
equivalent to \(500/2^{8}=1.953125\). Obviously, in integer arithmetic
that would always truncate to 1, but by starting with an upscaled value
of *carry, an upscaled value of the new carry factor
(\(1.953125*2^{\mathtt{shift}}\)) is produced.

In the next iteration, the next value of \texttt{fbk\_mdev} passed to the
function as \texttt{arg} is multiplied by the new upscaled carry factor
in the line \texttt{arg *= *carry}, which produces an upscaled result.
Two lines later, the integer log of this upscaled and factored up value
is taken. Continuing the example case with \(\mathtt{fbk\_mdev} =
500\,\mu\)s, the \texttt{ilog2()} function is applied to
\(500*(1.953125<<\mathtt{shift})\) before shift is subtracted, which
produces 9 not 8. As the process iterates, if a steady state of
\(\mathtt{mdev} = 500\,\mu\)s were to remain, the output of the integer
log would be 8 only 3.422\% of the time, while it would be 9 the other
96.578\% of the time (thus averaging to \(\mathrm{lg}(500)\), which is
8.96578...) .

Even with geometric carry implemented, the average of all the integers
logs still understates the value relative to the true floating point log.
This is because every right shift to remove the extra degree of upscaling
truncates the output. To counteract this truncation bias, 0.5 is added
before the downscaling, because 0.5 is the average of all the possible
values of the truncated bits. Of course, 0.5 is upscaled by
\texttt{shift} before being added, to match the upscaling of the
\texttt{arg} that it is added to.

\paragraph{Precision:} When the EWMAs are calculated, \texttt{fbk\_srtt}
and \texttt{fbk\_mdev} are upscaled by their respective \(g\) shifts.
However, when their logs are taken, copies of these stored upscaled
values are taken and reverted to their non-upscaled values. Instead, the
carry factor is upscaled by the \(g\) shift. Thus, the same number space
is needed for upscaling, but the values of the EWMAs need two additional
bits on top of their usual maximum size (\(<2^24\) as outlined earlier).
These two bits consist of: \begin{itemize} \item The additive adjustment
of 0.5 that unbiases the integer log truncation, which consumes one bit
in the worst-case.
	
\item The unscaled value of the geometric carry factor, which takes a
value between \((1\le \mathtt{carry}<2)\), which also consumes 1 bit.
\end{itemize} Thus, \(42+2=44\) bits are needed for the local variable
holding the factored up and adjusted EWMA (\texttt{arg}) within the
\texttt{carry\_ilog2()} function.

% ----------------------------------------------------------------
\subsection{Combining logs}\label{ecn-fallbacktr_log_combine}

In the passive detection algorithm, both the depth (d) and variability
(v) terms involve a log function. So in practice, with
judicious choice of the parameters V and D, they could be combined
efficiently. For instance, if \texttt{V=D}, then\\
\-\qquad\texttt{V*lg(v/V0) + D*lg(max(d/D0,1))}\\
is equivalent to\\
\-\qquad\texttt{( d>D0 ? V*lg(v*d/(V0*D0)) : V*lg(v/V0) )}

Then the `Per RTT' block of pseudocode above would be replaced with that below, which aggregates two lots of steps into one.

\begin{verbatim}
// Stored variables
    fbk_ewmas_carry;        // Replaces fbk_srtt_carry

{   // Per RTT
    // Temporary variables for readability
    u64 fbk_mdev_;
    u64 fbk_depth_;
    u64 fbk_ewmas_;
    int fbk_ewmas_lg_;

    fbk_mdev_ = fbk_mdev>>fbk_g_mdev_shift;             // Remove upscaling
    fbk_depth_ = fbk_srtt>>fbk_g_srtt_shift - rtt_min;  // Smoothed q depth w/o upscaling

    if (fbk_depth_ > D0) {
        fbk_ewmas_ = fbk_mdev_ * fbk_depth_;    // Product of EWMAs
        fbk_ewmas_lg_ = carry_ilog2(fbk_ewmas_, fbk_g_mdev_shift, *fbk_ewmas_carry)
        // V*lg(v*d/(V0*D0)), assuming V = D
        classic_ecn += (u64)fbk_ewmas_lg_<<(PRAGUE_ALPHA_BITS - V);
        classic_ecn -=  V0_LG_US + D0_LG_US;
    } else {
        fbk_mdev_lg_ = carry_ilog2(fbk_mdev_, fbk_g_mdev_shift, *fbk_mdev_carry)
        // V*lg(v/V0)
        classic_ecn += (u64)fbk_mdev_lg_<<(PRAGUE_ALPHA_BITS - V);
        classic_ecn -=  V0_LG_US;
    }
}
\end{verbatim}
