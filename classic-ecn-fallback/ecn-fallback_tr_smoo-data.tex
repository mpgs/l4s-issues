% !TeX root = ecn-fallback_tr.tex
% ================================================================
\section{Adapting the RTT Smoothing Timescale}\label{ecn-fallbacktr_srtt_adapt}

Given the EWMA of the RTT is meant to be smoothed over a sawtooth, and it is updated per ACK, na\"{\i}vely, one would think the smoothing gain should be the reciprocal of the average number of ACKs in a TCP sawtooth, \(n_a\).  However, there are three reasons not to take this na\"{\i}ve approach:
\begin{itemize}
	\item The whole reason for trying to move away from Classic congestion
	controls is that the average duration of a sawtooth grows linearly with
	the congestion window. So, if the smoothing gain tracked \(n_a\), the
	classic ECN AQM detection algorithm would become linearly more sluggish
	as the congestion window scaled up.
	
	\item In practice, the duration of a Classic sawtooth does not actually
	grow linearly as link rate scales, because the longer it takes, the more
	likely it will mistake some other disturbance for congestion, such as a
	competing short flow or a bit error.\footnote{For the same reason, as link capacity
	scales, it is generally accepted that there is no need to try to maintain
	`fairness' with a congestion control that increasingly underutilizes the
	available capacity all by itself, meaning that its sawteeth do not
	continue to grow to full utilization, instead collapsing and restarting
	long before they have reached their theoretically `fair' amplitude.}
	
	\item Classic ECN AQM detection is only needed for the transition from
	Classic to scalable (L4S) congestion controls. If the L4S experiment is
	successful, there will be no need to scale the transition mechanism 
	beyond the flow rates Classic congestion controls can attain today.
\end{itemize}

Therefore, a formula for the average number of ACKs in a TCP sawtooth
\(n_a\) will be derived, but then the degree by which it scales with
congestion window will be reduced using empirical experience, to
produce a heuristic that works in practice.

\begin{align}
	n_a	&\approx \mathrm{avg\_packets\_per\_sawtooth} / d\notag\\
			&\approx  1 / (p*d),\label{eqn:ecn-fallbacktr_n_a_wrt_p}
\end{align}
where \(d\) is the delayed ACK ratio (typically 2) and \(p\) is the
probability of an ECN mark, because a mark ends a sawtooth and on average
there will be one mark per \(1/p\) packets.

It would be efficient to only recalculate the gains to use for
\texttt{srtt} and \texttt{mdev} at the end of every sawtooth, when the
slow-start threshold (\(S\)) is recalculated. We relate \(p\) to the
average congestion window \(W\) using the steady state response function
of a Classic congestion control, taking Reno as the worst
case~\cite{Mathis97:TCP_Macro}. Then it will be straightforward to relate
\(W\) and therefore \(p\) to \(S\).
\begin{align}
	W_{\mathrm{Reno}} &\approx \sqrt{\frac{3}{2p}}\notag\\
\intertext{Substituting for \(p\) in \autoref{eqn:ecn-fallbacktr_n_a_wrt_p}}
	n_a &\approx 2W_{\mathrm{Reno}}^2/3d\label{eqn:ecn-fallbacktr_n_a_wrt_W}\\
\intertext{For Reno, the average congestion window lies midway between 
\(S\) and \(S/\beta\), where \(\beta\) is the reduction factor of the congestion control:}
	W_{\mathrm{Reno}} &\approx (1+1/\beta) / 2 * S_{\mathrm{Reno}}\notag\\
\intertext{Substituting for \(W_{\mathrm{Reno}}\) in 
\autoref{eqn:ecn-fallbacktr_n_a_wrt_W}}
	n_a &\approx (1+1/\beta)^2 / (6d) * S^2\notag\\
		   &\approx 3 / 4 * S^2,\notag
\end{align}
assuming a typical delayed ACK ratio, \(d = 2\), and Reno as the
worst-case congestion control where \(\beta_{\mathrm{Reno}}=0.5\).

For efficiency, the smoothing gain should be an integer power of 2, so
that the integer power can be used as a bit-shift. A fast integer base 2
log (such as ilog2()) rounds by truncation. Therefore, for unbiased
rounding, \(n_a\) would need to be scaled up by 3/2 before taking the
log. Thus, the integer log of \(3/2*3/4*S^2 \approx S^2\) would be taken.

As explained above, in practice an empirical formula for the smoothing
gain is used in place of this theoretical formula. Nonetheless, this
theoretical number of ACKs per sawtooth helped set an upper bound to the
range of formulae to be tried in experiments. The formula for smoothing
gain, \(g_{\mathrm{fbk_srtt}}\), used in experiments was of the form:
\[g_{\mathrm{srtt}} = U2 * S^{U1}.\]

In experiments with a range of link rates between 4\,Mb/s and 200\,Mb/s
and RTTs between 5\,ms and 100\,ms, the parameters for this formula that
resulted in a good compromise between precision and speed of response
were: \[g_{\mathrm{srtt}} = 2 * S^{3/2}.\]

Currently the mean deviation is smoothed twice as slowly as this, i.e.:
\[g_{\mathrm{mdev}} = g_{\mathrm{srtt}} * 2.\]

%Because deviation tracks the absolute error, it only needs a little more
%than half a sawtooth to converge on an average that will remain roughly
%the same as the full sawtooth continues. Therefore, to improve
%responsiveness, the mean deviation is smoothed twice as fast as the RTT
%itself: \[g_{\mathrm{mdev}} = g_{\mathrm{srtt}} / 2.\]
\bob{Currently, the Linux code and the pseudocode uses g\_mdev = g\_srtt * 2}
