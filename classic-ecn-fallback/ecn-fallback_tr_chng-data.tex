% !TeX root = ecn-fallback_tr.tex
% ================================================================
\section{CC Behaviour Changeover Algorithm}\label{ecn-fallbacktr_changeover}

% ----------------------------------------------------------------
\subsection{TCP Prague-Based Example}\label{ecn-fallbacktr_ex_dctcp}

\begin{figure}[h]
  \centering
  \includegraphics[width=0.45\linewidth]{c-trans-behaviour}
  \caption{Visualization of the Transition between Scalable and Classic behaviour.}\label{fig:ecn-fallbacktr_c-trans-behaviour}
\end{figure}

The proposed changeover algorithm transitions its response to ECN from
scalable to ABE-Reno as \texttt{c} transitions from 0 to 1, as visualized in \autoref{fig:ecn-fallbacktr_c-trans-behaviour}.

Alternative Backoff with ECN (ABE) is an Experimental RFC~\cite{Khademi18:ABE}
that suggests it is preferable for the reduction in cwnd to be less severe in
response to an ECN signal than to a loss. The logic is that loss is more
likely to emanate from a deep buffer, whereas any ECN signals are likely to be
emanating from a modern AQM which will be configured with shallow target
queuing delay. Therefore, it is reasonable to reduce less in response to ECN
in order to improve utilization. A downside with ABE is that it will lead to
ECN flows competing more aggressively with non-ECN flows, but the difference
is not so great that non-ECN flows would be severely disadvantaged.

It is easiest for TCP Prague to fall back to Reno or ABE-Reno (though falling back to a less
lame congestion control such as Cubic or BBRv2 would be preferable). On an ECN
signal, the ABE RFC recommends a reduction to \(\mathrm{beta_{ecn}}\) of the
original cwnd, where for Reno \(\mathrm{beta_{ecn}}\) is in the range 0.7 to
0.85. The pseudocode below uses \(\mathrm{beta_{ecn}} =
0.7\). If ABE were disabled, for Reno it would be appropriate to transition
using \(\mathrm{beta_{ecn}} = \mathrm{beta_{loss}} =0.5\), but this detail is
not shown in the pseudocode.

Note that the maximum of Prague's variable reduction is 0.5, whereas
ABE's fixed reduction is less than 0.5 (it is 0.3 in our pseudocode).
Therefore, although Prague's reduction will usually be smaller than
ABE's, it can also be larger during periods of high congestion marking.

The example pseudocode below modifies Prague's congestion window reduction, by making
it a function of \texttt{alpha} and \texttt{c}, where:
\begin{itemize}
	\item \texttt{alpha} is already used in DCTCP and TCP Prague to hold an
	EWMA of the congestion level. 
	
	\item  \texttt{c} is the detected \texttt{classic\_ecn} score clamped between 0 and 
	1 (in floating point pseudocode)
\end{itemize}

Two types of simple algorithm are conceivable. 
They are compared in the two alternative statements to calculate
\texttt{reduction} following the original statement used by DCTCP in the
pseudocode below:

\begin{verbatim}
#define BETA_ABE 0.7                     // ABE: Alternative Backoff with ECN [RFC8511]
#define ALPHA_ABE 2*(1-BETA_ABE)         // 0.6

// For pseodocode clarity, c is a float covering the classic ECN transition (Section 3)
c = min(max(classic_ecn / CLASSIC_ECN}, 0), 1);

// original DCTCP reduction within prague_ssthresh()
reduction = cwnd * alpha / 2;

// reduction alternative #1
reduction = cwnd * (alpha + c * (ALPHA_ABE - alpha)) / 2;

// reduction alternative #2
reduction = cwnd * max(alpha, c * ALPHA_ABE) / 2;
\end{verbatim}

The macro \texttt{ALPHA\_ABE} is just the value that, when halved, would limit
the multiplicative reduction of cwnd to \texttt{BETA\_ABE}, by the formula:
\texttt{reduction = cwnd * BETA\_ABE = cwnd * (1 - ALPHA\_ABE / 2)}

\begin{figure}
  \centering
  \includegraphics[width=0.49\linewidth]{cc-changeover-linear}
  \includegraphics[width=0.49\linewidth]{cc-changeover-log}
\caption{Comparison of Two Alternative CC Behaviour Changeover Algorithms
while checking a sweep across the full range of \texttt{c} and
extreme values of \texttt{alpha}; linear (left) or log-scale
(right)}\label{fig:ecn-fallbacktr_cc-changeover}
\end{figure}

To quickly check all possible combinations,
\autoref{fig:ecn-fallbacktr_cc-changeover} shows an example time series
of \texttt{alpha} with extremely low and high values as it interacts with
a sweep of all the values of \texttt{c}, including plateaus at 0 and 1. It shows
the CC reduction on a linear
and log scale for the two alternative changeover algorithms.

Alt\#1 gives the reduction a pro-rata contribution from each of alpha and
\texttt{c}, dependent on the value of \texttt{c}. Alt\#2 takes the simple
maximum of \texttt{alpha} and the value of \texttt{c} scaled down by
\texttt{ALPHA\_ABE}.

As flow rates scale, the typical value of \texttt{alpha} becomes very small,
so it is deceptive to focus on the rounds when \texttt{alpha} is high.
Nonetheless, in current networks \texttt{alpha} can approach 100\%. With
either alternative, when \texttt{alpha} is small, the log plots show that
\texttt{c} dominates over most of its range. However, on the left of the log
plot it can be seen that \texttt{alpha} dominates when \texttt{c} is close to
zero.

Alt\#1 behaves more in the spirit of a transition, because it takes pro-rata
contributions from each approach. Whereas Alt\#2 is more like a binary
switch-over. However, the difference is very subtle and unlikely to be
noticeable by end-users.

Both alternatives can lead to a reduction greater than \texttt{APHA\_ABE} (at
about round \#400 in \autoref{fig:ecn-fallbacktr_cc-changeover}). This effect
is less severe with Alt\#1, but it not necessarily a bad thing to reduce cwnd
by more than \texttt{APHA\_ABE} when congestion is high.

\paragraph{Subsection Summary:} Ultimately, the two alternatives are similar 
enough that the choice between
them can be made on simplicity grounds, in which case Alt\#2 is slightly
preferable.

% ----------------------------------------------------------------
\subsection{Transition of ECT marking?}\label{ecn-fallbacktr_ect_marking}

When the CC transitions from scalable to classic, should the marking of
packets transition from ECT1 to ECT0?

Let us consider a bottleneck with each type of AQM in turn:
\begin{description}
	\item[Classic AQM:] The only concern here is the sender's CC behaviour, not
	its packet markings. If the CC does not transition to classic behaviour, it
	might outcompete classic flows (if the bottleneck is not FQ). But, it makes no
	difference whether the sender marks the packets ECT0 or ECT1. Because
	'classic' means RFC 3168, and RFC 3168 requires an AQM to treat ECT0 and ECT1
	identically.
	
	\item[L4S AQM:] Here both the packet markings and the CC behaviour need to
	comply with the L4S spec.~\cite{Briscoe15f:ecn-l4s-id_ID} in order to achieve
	any L4S performance benefit. If packets are not marked ECT1, they will never
	be classified into an L4S queue.
\end{description}

Therefore, it is not a good idea for an L4S-capable CC to transition packet
markings to ECT0, even if it transitions to classic CC behaviour (because it
detects a classic ECN bottleneck). It does no harm to anyone by marking its
packets ECT1. But if it uses ECT0, then if the bottleneck moves to one that
supports L4S~\cite{Briscoe15e:DualQ-Coupled-AQM_ID}, its packets will be
classified into the classic queue and it will never detect the lower delay
variability that would trigger its transition back to L4S.

Note that a classic ECN-capable CC does not harm other flows in an L4S
queue\footnote{In contrast to a non-ECN-capable classic CC, which overruns the
shallow ECN threshold until it detects tail drop}; it just unnecessarily
under-utilizes capacity on its own and competes lamely with L4S flows.

\paragraph{Subsection Summary:} A sender that is L4S-capable should always set its
packets to ECT1, irrespective of whether it has transitioned to classic CC
behaviour.


