% !TeX root = ecn-fallback_tr.tex
% ================================================================
%\section{Related Work}\label{ecn-fallbacktr_related_work}
\bob{ToDo: Related Work}

% ================================================================
\section{Conclusions}\label{ecn-fallbacktr_conc}

This paper has explained and justified the design of an algorithm that
can be used by scalable congestion controls to fall back to Classic
(Reno-friendly) behaviour on detection of a Classic ECN AQM.
This is intended to ensure that no Classic
flow is beaten down by a scalable congestion control such as TCP Prague
in any Classic ECN bottlenecks that might be operational on the Internet.

Since the v1 algorithm design, the passive part of the algorithm has been implemented and extensively
evaluated over a wide range of link-rates, RTTs and traffic scenarios.
This paper now describes v2 of the algorithm. The only change 
has been to add adaptive smoothing of the RTT EWMAs, and to
set or slightly alter the setting of a number of the parameters.

The algorithm has proven itself able to make the Prague congestion
control fall back to Classic ABE-Reno behaviour whenever it proves
necessary due to a Classic AQM at the bottleneck. The resulting ratio of
the throughputs of Prague and Cubic-ECN flows easily falls within the
range of about 0.5 to 2. In contrast, without the algorithm, the
ratio can be as high as 16. 

In no test scenario has the throughput of a Prague flow been persistently
greater than twice that of a Cubic-ECN flow. Thus the algorithm satisfies 
the `Prague L4S' requirement to
fall back to Reno-friendly behaviour when operating over a Classic ECN
AQM (at least a shared one, and ideally any such AQM).
It is therefore a
candidate for use as a reference ECN fall-back design in other scalable
congestion controls.

Although so far evaluation has only been carried out with CoDel as the
Classic AQM, CoDel was chosen as the likely worst-case; being least
distinguishable from an L4S AQM. This gives some confidence that planned
evaluation with other Classic AQMs will be successful.

% ================================================================
\section{Future Work}\label{ecn-fallbacktr_future}

The evaluation in \S\,\ref{ecn-fallbacktr_evaluation} focuses on the v2 passive
detection algorithm based on delay variation in
\S\,\ref{ecn-fallbacktr_passive_detection}. It shows promise. But it gives many
false positives at link rates below
12\,Mb/s. Also at higher link rates some false positives appear as base RTT
exceeds about 80\,ms. Initial testing of ideas for further parameter
tuning and heuristic adaptation of the thresholds were more promising
(see footnotes \textsuperscript{\ref{note:eval_heuristic_plan} \&
\ref{note:eval_smoothing_plan}}). So it was originally planned to fully explore these
avenues.

Then a large number of tests over a wider range of scenarios was planned, 
for instance:
\begin{itemize}
	\item with mixed RTTs, and emulating applications that sometimes
	self-limit the window; \item over different Classic AQMs that might be
	deployed in shared queues on the Internet (COBALT, DOCSIS PIE, RED);
	
	\item over different L4S AQMs, e.g.\ FQ\_CoDel with a shallow ECN
	threshold, DualPI2 with a ramp instead of a step;
	
	\item with different combinations of TCP Prague features enabled or
	disabled, e.g.\ RTT independence, paced chirping;
\end{itemize}

After all that, it would still be necessary to address the interaction of delay-variation measurements
with radio links, and links that add burstiness by aggregating frames.

Therefore, the focus shifted to using discrete metrics rather than delay
variation. Design ideas and a feasibility experiment on using the spacing
between marks have been written up in \S\,\ref{ecn-fallbacktr_inter-mark}. And
an idea called `exclusive' marking for altering the specification of L4S to make
it more distinct from Classic ECN marking is written up in
\S\,\ref{ecn-fallbacktr_active_solution2}. Although these approaches still
require empirical validation, the range of experiments needed should be far narrower
and less onerous.

Since the v2 algorithm was evaluated, there has also been a shift of focus
towards out-of-band testing; to validate paths prior to L4S deployment with
occasional reviews afterwards. A new section on out-of-band testing has been
added at \S\,\ref{ecn-fallbacktr_OOB_active}, where a very simple but adequate
test is defined. If out-of-band testing is deemed sufficient, nothing more would
be necessary than to verify that simple test.

% ================================================================
\section{Acknowledgements}\label{ecn-fallbacktr_acks}

Transitioning gradually from scalable to classic behaviour, using ECT0 packets
for active detection and using the spacing between marks were based on initial
ideas suggested by Koen De Schepper. The ideas based on altering the
standardization of L4S AQMs (\S\,\ref{ecn-fallbacktr_active_solution2}) were
suggested by Alex Burr\footnote{In an email posting to the IETF's tsvwg mailing
	list:
	\url{https://mailarchive.ietf.org/arch/msg/tsvwg/mIqNOa5vmIjFk_LNdOLJHQfm5s8/}}.
Thanks are due to Olivier Tilmans and Koen De Schepper for reviewing the design
approach and the code, and to Tom Henderson for advice on presentation of the
results.

Bob Briscoe was part-funded by the Comcast Innovation Fund. The views expressed
here are solely those of the author.
