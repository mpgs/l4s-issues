% !TeX root = ecn-fallback_tr.tex
% ================================================================
\section{Passive Detection of Classic ECN AQMs}\label{ecn-fallbacktr_detection}

% ----------------------------------------------------------------
\subsection{Candidate Metrics}\label{ecn-fallbacktr_metrics}

The following metrics are likely to be relevant when detecting a classic ECN AQM:
\begin{itemize}
	\item The onset of CE marking;
	\item Application-limited (no buffering at the sender);
	\item Receive window limited (\texttt{rwnd} \(<\) \texttt{cwnd});
	\item The moving mean deviation of the RTT (\texttt{fbk\_mdev})\footnote{An alternative to
	standard deviation that is a little easier to compute and no less valid as a
	variability metric~\cite[Appx A]{Jacobson88b:Cong_avoid_ctrl}} of the RTT.
	\item The difference between the smoothed RTT (\texttt{fbk\_srtt}) and a 
	minimum RTT (\texttt{rtt\_min}), with suitable
	safeguards against a false minimum and against step changes;
	\item The distribution of the spacing between ECN marks
\end{itemize}

Note that the variable names \texttt{fbk\_mdev} and \texttt{fbk\_srtt} are prefixed with \texttt{fbk\_} to emphasize that they are specific to the fall-back algorithm, and not necessarily the same as the \texttt{mdev} and \texttt{srtt} variables that Linux TCP already maintains for its retransmit timer (see \S\,\ref{ecn-fallbacktr_pseudocode_srtt}).

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\subsubsection{Dependence on Presence of CE marking}\label{ecn-fallbacktr_CE_presence}

Obviously no transition to classic should occur unless there has been a CE
mark. Any transition should be suppressed for a number of RTTs after the onset of CE
marking, both to allow the connection to stabilize and because aggressive
competition for bandwidth is not a great concern with short flows. Indeed when
rate `fairness' is considered over time, it is more fair if long-running flows are
less aggressive than short flows~\cite{Guo02:Size-based_scheduling,
Yang02:Sized-basedTCP, Zielger04:TCP_Vienna, Moktan12:FavoringShort,
Bai15:PIAS, Briscoe19c:FQ_v_e2e}---though a delay is sufficiently motivated by the need for a
stabilization period rather than any desire to use a different form of
fairness.

\begin{description}
	\item[How long for instability to end?] A classical slow-start ends on the
	first CE (unless it had already ended due to a loss). In the next few rounds,
	all the flows suffer a period of instability as they recover from the
	transient overshoot of the new flow. The random nature of this period leaves
	them all at different shares of capacity. Then they might take a few hundred
	further round trips to converge on stable shares. It is likely that any flow-start
	approach developed for shallow threshold ECN might have a less clear cut
	transition between a flow-start phase and a period of convergence. Given this
	likely heterogeneity in approaches to flow-start, it is not feasible to
	quantify how long any single flow should wait after the first CE before
	starting to detect a classic ECN AQM, because the period of instability
	depends more on the behaviour of other flows than on its own behaviour.

	Therefore it is proposed to start maintaining metrics as soon as the first
	feedback of a CE mark arrives, rather than attempting to wait for the
	subsequent instability to subside. As will be seen next, even if the RTT
	metrics start during this period of instability, they can be given plenty of time
	to stabilize before they alter the CC behaviour, while they remain scalable in 
	the `sticky' region.

	\item[How long before inappropriate convergence becomes significant?]
	In the original paper on TCP Cubic\cite{cubic}, convergence was still described 
	as 'short' when it took one or
	two hundred rounds (assuming flows even last that long). Therefore, relative to
	overall convergence time, it will be insignificant if a flow takes a couple of
	dozen rounds to work out whether it should be converging to an L4S or to a
	classic target.
\end{description}

Rather than take an absolute number of rounds before the CC behaviour starts
to transition, it would be better to depend on how strongly the other metrics are
indicating that a transition is necessary. For instance, the higher RTT
variance is, the fewer rounds would
need to elapse before allowing a transition to start.

If CE marking stops for a protracted period, it will be likely that a non-ECN
link has become the bottleneck. Then the choice between classic and scalable
ECN behaviour would be moot and the default loss response would be sufficient.
If CE marking were to pick up again later, it would be best to ignore (i.e. not
measure) any period with more than one loss but no  CE marking, then restart the 
detection algorithm if a CE mark ever appears again.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\subsubsection{Dependence on being Self-Limited}\label{ecn-fallbacktr_self-limited}

If a TCP Prague flow is app-limited or receive-window limited (i.e.
self-limited), there is no great need to fall back to classic behaviour on
receipt of an ECN mark.

The presence of CE marks while the flow is not
trying to fill the pipe (its send buffer is empty\footnote{With the
introduction of pacing, it is no longer correct to measure whether a flow
is network limited by whether it is fully using the congestion window. It
is necessary to check whether the send buffer is empty instead.})
probably implies that a greedy
flow or other short flows are sharing the link. Then (assuming cwnd
validation is being used) the flow will not be increasing cwnd as much as
competing traffic could be. In that case, a large classic response to a
CE-mark could under-utilize the link until cwnd returned. So a small scalable
response would be more appropriate.

Also, any tendency towards classic behaviour due to RTT variability (see below) will be 
more due to other flows. So 
the classic ECN variable ought to reduce by a certain amount per RTT while a
sender is self-limited. However, not being self-limited alone is not a reason
to increase the variable---for that there has to be a positive sign of a
classic ECN AQM, such as RTT variabiity.

Similarly, while the sender is idle, any previous detection of a classic ECN
bottleneck could become stale. However, during an idle period there are no
events to trigger any actions, so an adjustment to the classic ECN variable will have to be
made at the restart of activity based on TCP's idle timer.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\subsubsection{Dependence on RTT Variability}\label{ecn-fallbacktr_RTT_deviation}

A large degree of RTT variability is the surest way to detect a
classic-ECN bottleneck. So, if accompanied by CE marking it is likely to imply
a classic ECN AQM at the bottleneck. For the Internet, 'large variability' can be
quantified as more than about 1.5\,ms of variability, given the target L4S delay
will generally be 1\,ms or less while the lowest target delay to which classic
AQMs are recommended to be configured is about 5\,ms. So any classic queue
could vary from zero to slightly above that.

Pseudocode for dependence of classic ECN fall-back on RTT variability will be given in
\S\,\ref{ecn-fallbacktr_passive_detection}. But first, the following two
subsections will discuss possible false positives and false negatives.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\paragraph{Non-queuing causes of RTT variability with an L4S bottleneck}\label{ecn-fallbacktr_false_positives}\leavevmode\\

RTT variability can have other causes than queuing:
\begin{itemize}
	\item A reroute.
	\item Variability in Interrupt handling, processor scheduling and batched
	processing by the endpoints and by nodes on the path. 
\end{itemize}

\paragraph{Reroute:} A moving average of RTT and deviation of the RTT from
this average does not filter out step changes in the base RTT
(e.g.\ due to a reroute), which could cause the moving average to be
temporarily 'incorrect' so that the mean deviation from this incorrect average
would temporarily expand (see \autoref{fig:ecn-fallbacktr_reroute}). The pseudocode in
\autoref{ecn-fallbacktr_alt-srtt} is intended to fill that gap.

\paragraph{Other Non-Queue Variability:} The passive detection algorithm assumes that the
combined result of all these variations will be small compared to variability
of a classic ECN queue. This assumption has turned out to be sufficient in testing so far. 
But it will need to be tested in a wider range of scenarios and parameters
altered accordingly (\S\,\ref{ecn-fallbacktr_detection_params}). 
If necessary active detection will need to be added, which is designed to 
complement passive detection where this assumption breaks down.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\paragraph{Low RTT variability with a classic ECN bottleneck}\label{ecn-fallbacktr_false_negatives}\leavevmode\\

RTT variability will not distinguish a classic ECN bottleneck in the following cases:
\begin{itemize}
	\item A high degree of flow multiplexing at a shared-queue bottleneck with a
	classic ECN AQM. The averaging effect of large numbers of uncorrelated
	sawteeth causes the mean deviation of the RTT of \(N\) flows sharing a buffer
	to be about \(1/\sqrt{N}\) of that of 1 flow, derived straightforwardly from
	the Central Limit Theorem~\cite{Appenzeller04:Sizing_buffers}.
	\item ...any others?
\end{itemize}

Few networks are designed so that sharing at a link serving a large number of
individual flows is controlled by the end-points, let alone with a classic ECN
AQM at this link as well. Nonetheless, we will consider three cases where this
could possibly occur:
\begin{description}
	\item [Commercial ISP's access link with a shared-queue classic ECN AQM:]
	Invariably, the operator designs the network so that the bottleneck is in the
	access link allocated to each customer, the capacity of which is isolated from
	other customers using a scheduler. For the mean deviation of a flow to appear
	to be 5\(\times\) lower (e.g.\ \(800\,\mu\)s rather than 4\,ms), at least 25 classic
	flows would have to be multiplexed together, by the Central Limit formula
	above. Such a scenario can occur within a single customer's access. However,
	for the fall-back algorithms of each flow to be fooled into thinking the
	bottleneck was L4S, that many classic flows would all have to run continually
	with no disruption from other flows. We have to accept that the algorithm
	could give a false negative in such a scenario, which is unlikely but
	possible.
	
	\item [Commercial ISP's core or peering link with a shared-queue classic ECN
	AQM:] The bottleneck can sometimes shift to a core link or more likely a
	peering point, where there per-customer scheduling is unlikely to be deployed and 
	flow multiplexing will be high enough to keep RTT very
	smooth. Usually this occurs during some sort of anomalous conditions, e.g.\ a
	provisioning mistake, a core link failure or a DDoS attack. If it does, the
	concern is that L4S flows could out-compete classic flows. Nonetheless, the
	scope for a high degree of flow rate inequality is very limited, as explained
	in \autoref{ecn-fallbacktr_common}.
	
	\item [Campus network access link:] A corporate or University network is
	rarely designed with an individual bottleneck for each user. Rather, each user
	typically has high speed connectivity to the campus (e.g.\ 1Gb/s Ethernet) and
	all stations using the Internet at any one time bottleneck at the campus
	access link(s) from the Internet. In such an access link, L4S flows will not
	coexist well with classic flows (as in
	\autoref{fig:ecn-fallbacktr_rate_flow}). It is not known whether any campus
	networks use classic ECN AQM in their access link, but they might do. Until
	the operator of such an AQM can deploy an L4S AQM, an unsatisfactory
	work-round would be to reconfigure the AQM to treat ECT(1) as Not-ECT so that
	it uses drop not CE as a signal for L4S flows. However, this would
	disincentivize L4S deployment in the affected campus networks. The 
	alternative of some campus networks just allowing the unfairness 
	would also be an option (applications already open multiple flows to
	achieve a similar advantage in the access to existing campus networks).
\end{description}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\subsubsection{Dependence on Minimum RTT}\label{ecn-fallbacktr_RTT_min}

Minimum RTT metrics are known to be problematic, especially where the buffer
is already filled by other traffic before a flow arrives. Therefore, it may be
preferable not to use this metric at all, and rely solely on RTT variability.

Nonetheless, it would do no harm to use a min RTT metric, as long as the
outcome was asymmetric. In other words, a large difference between
\texttt{fbk\_srtt} and \texttt{srtt\_min} would make classic fall-back more likely,
while a small difference would not make classic fall-back less likely. This is 
the approach taken in the pseudocode below.

\paragraph{Subsection Summary:} \autoref{fig:ecn-fallbacktr_metric-push-pull} depicts the three main variables that will be used to drive Classic ECN AQM detection. It shows that queue variability should be able to increase or decrease the \texttt{classic\_ecn} score symmetrically. In contrast queue depth will be limited to only increasing the score, due to the well-known problem of measuring a false \texttt{rtt\_min}. The degree to which a flow is self-limiting is also asymmetric, but in this case it can only decrease the score.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.45\linewidth]{metric-push-pull}
  \caption{Visualization of the three main metrics, and how their effect on the \texttt{classic\_ecn} score will be constrained}\label{fig:ecn-fallbacktr_metric-push-pull}
\end{figure}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\subsubsection{Dependence on Spacing Between ECN Marks}\label{ecn-fallbacktr_inter-mark}

This metric was only first thought of after v2 of the algorithm had been
implemented and evaluated. At present it is just an idea, but it seems the most
promising approach. It might prove to be the only metric that is needed, which
would provide a really simple solution. Otherwise, it would complement the other
metrics above.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{ce-space-distrib}
	\caption{Distributions of spacing between marks for four different AQMs}\label{fig:ecn-fallbacktr_ce-space-distrib}
\end{figure}
The idea is to record the number of non-marked packets between one ECN mark to
the next, and monitor the distribution of different spacings. At its simplest it
might be sufficient to monitor the prevalence of just one selected spacing
(e.g.\ 1, meaning a mark every other packet) relative to the total number of
marks. The initial experiments shown in
\autoref{fig:ecn-fallbacktr_ce-space-distrib} shows that all the low spacings
(except zero) are much higher for the L4S (DualPI2) AQM than the others, and
there is fairly sound reasoning for that, as explained next.

By definition, all Classic AQMs filter out short term fluctuations in the queue.
Most, if not all, known Classic AQMs also attempt to even out the spacings
between the markings to avoid clustering---a process sometimes called
derandomization. For instance:
`\begin{itemize}
	\item After RED calculates the marking probability, to prevent marks clustering it converts the marking probability into a uniform random variable (see \cite[\S\,7]{Floyd93:RED}).
	\item CoDel~\cite{Nichols18:codel_RFC} and any AQM derived from it (e.g.\ COBALT~\cite{Palmei19:Cobalt}) uses a control law that inherently introduces a measured space between markings.
	\item PIE~\cite{Pan13:PIE} and derivatives such as DOCSIS PIE~\cite[Appx.~M]{CableLabs:DOCSIS3.1} use a derandomization algorithm similar to that of RED.
\end{itemize}

In contrast, by definition, an L4S AQM is specifically precluded from smoothing
its markings~\cite[\S\,5.2]{Briscoe15f:ecn-l4s-id_ID}, which is the job of the
end system in the L4S architecture~\cite[\S\,4.4]{Briscoe15f:ecn-l4s-id_ID}.
Therefore, typically the distribution of the spacings will be geometric. A
geometric distribution has the counter-intuitive property that the greater the
spacing, the less likely it will occur (much like the DualPI2 distribution in
\autoref{fig:ecn-fallbacktr_ce-space-distrib}). So the most likely spacing is
always 0, the next most likely 1, and so on.

A higher prevalence of zero spacing is a special case, and not a useful
distinguishing feature, because any AQM might occasionally mark all packets for
a while when it overshoots.

\autoref{fig:ecn-fallbacktr_ce-space-distrib} is only an initial experiment
based on single runs of 20s duration including flow startup with one link rate
and one RTT (40\,Mb/s, 20\,ms) for two long-running flows started
simultaneously; one Prague and one Cubic ECN.

It is very likely that the picture is less clear-cut in the Internet at large,
although that will be hard to determine given no-one has yet captured a FIFO
Classic ECN AQM in the wild, so we do not have one to dissect in the lab. Below
some known complicating factors are discussed, some of which turn out not to be
of concern, others need further investigation, and some are truly unknown and
likely to remain so:
\begin{itemize}
	\item Different L4S Distributions:
	\begin{itemize}
		\item In a DualQ Coupled AQM, when there is a mix of L4S and Classic traffic,
the L4S traffic is marked by coupling from an internal probability (termed
\(p^{\prime}\)) pronounced p-prime, that also drives the Classic AQM. This
would seem to mean that the distribution of spacing of L4S marks could be
similar to that from the AQM in the Classic queue. However, on the Classic
side, \(p^{\prime}\) has to be squared then derandomized. While on the L4S
side \(p^{\prime}\) is merely factored up. So it would make no sense to derive
the coupled L4S probability from the derandomized Classic probability, which
would involve an unnecessary square root operation. This argument is very
likely to apply whatever algorithm is used for the Classic AQM.
		\item L4S AQMs often use a simple step threshold. So marking will not be
driven from a random variable, and therefore it will not follow a geometric
distribution. A pure step threshold is likely to cause its own distinctive
distribution of spacing, with long runs of solid marking followed by long runs
of none. However, different patterns are likely to result from different
levels of short flows in the background.
	\end{itemize}
	\item Different Classic Distributions (assuming FIFO Classic ECN AQMs do exist):
	\begin{itemize}
		\item Even if a Classic ECN AQM derandomizes marking in a FIFO, it only
derandomizes the spacing between marks in the aggregate, not in each flow. If
there are multiple flows the packets from each flow tend not to take perfect
turns as they arrive, so the spacing between markings in each flow becomes
more random again (it un-derandomizes)~\cite[\S\,5.6]{Briscoe15d:PIE_rvw}.
This could start to look more like the distribution of spacing from an L4S
AQM.
		\item It is possible that some Classic ECN AQMs will not include
derandomization code, for instance in high-speed switches to simplify the
hardware process.
		\item Large numbers of AQM designs have been proposed in research literature
and in patents. If any were implemented, they might not all have included
derandomization.
	\end{itemize}
\end{itemize}

If further experimentation proves that the spacing between marks still has merit
as a metric, it will need to be designed into an algorithm that increases the
\texttt{classic\_ecn} variable the more likely the AQM is Classic. And it will
need to take account of periods when the flow might be application limited,
receive-window-limited or idle. However, as already explained, the algorithm
described below was designed implemented and tested before using an inter-mark
spacing metric had been thought of.

% ----------------------------------------------------------------
\subsection{Passive Detection Pseudocode }\label{ecn-fallbacktr_passive_detection}

The following pseudocode pulls together all the passive detection ideas in the preceding sections.
\begin{verbatim}
/* Parameters */
#define V 0.5          // Weight of queue *V*ariability metric
#define D 0.5          // Weight of mean queue *D*epth metric
#define S 0.25         // Weight of *S*elf-limiting metric
#define C_FRAC_IDLE 2  // Multiplicative reduction in classic_ecn each idle timeout
#define CLASSIC_ECN 1 // Max of transition range for classic_ecn score
#define L_STICKY 16*V  // L4S stickiness incl. min rounds from CE onset to transition
#define C_STICKY 16*V  // Classic stickiness
#define V0 750         //  Reference queue *V*ariability [us]
#define D0 2000        // Reference queue *D*epth [us]

/* Stored variables */
classic_ecn; // Signed integer. The more +ve, the more likely it's a classic ECN AQM
rtt_min;    // Min RTT (using Kathleen Nichols's windowed min tracker in Linux)
fbk_srtt;           // The smoothed RTT (see later pseudocode)
fbk_mdev;           // The mean deviation of the RTT (see later pseudocode)
s;           // Proportion of the latest RTT that was self- (app- or rwnd-) limited

// Temporary variables to improve readability
v = fbk_mdev;
d = fbk_srtt - rtt_min;           // The likely mean depth of the queue. 
delta_;

/* The following statements are intended to be triggered by the stated events */

{   // On connection initialization
    classic_ecn = -L_STICKY;
}

{   // On CE feedback, enable delta_ calc'n if classic_ecn is clamped at its minimum 
    classic_ecn += (classic_ecn <= -L_STICKY);
}

{    // On expiry of idle timer
    if (classic_ecn > 0) {
        classic_ecn = classic_ecn/C_FRAC_IDLE;
        re-arm_idle_timer();
    }
}

{   // Per RTT
    if (classic_ecn > -L_STICKY) {    // Suppress delta_ calc'n if classic_ecn at min
        delta_ = V*lg(v/V0) + D*lg(max(d/D0, 1)) - S*s;
        classic_ecn = min(max(classic_ecn  + delta_, -L_STICKY), C_STICKY);
    } else {
        ect_tracers = 0;    // Unsuppress ect_tracers (for active detection in Section 5)
    }
}

{   // Per ACK
    // Update fbk_mdev and  fbk_srtt (see later pseudocode)
}
\end{verbatim}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\paragraph{Passive Detection Pseudocode Walk-Through}

While \texttt{classic\_ecn} sits at \texttt{-L\_STICKY}, calculation of
\texttt{delta\_}, the change in \texttt{classic\_ecn}, is suppressed to save
unnecessary processing. Maintenance of the variables used in this calculation
could also be suppressed (not shown).

At connection initialization, maintenance of the \texttt{classic\_ecn}
variable starts off in the above quiescent state. Feedback of a CE mark awakens it
by incrementing \texttt{classic\_ecn} by its minimum integer granularity (1).

Every RTT, as long as \texttt{classic\_ecn} is not in its quiescent state, the
per-RTT change in \texttt{classic\_ecn} is calculated. This is the core of the
passive classic ECN detection algorithm. To aid readability, a
temporary variable (\texttt{delta\_}) is assigned to this intermediate
calculation.

The change in \texttt{classic\_ecn} consists of three terms, each weighted
relative to each other by the three parameters \texttt{V}, \texttt{D} and
\texttt{S}: 
\begin{description} 
	\item[RTT Variability, \texttt{v} (\S\,\ref{ecn-fallbacktr_RTT_deviation}):]
	The metric \texttt{lg(v/V0)} is used, where lg() is an approximate (fast)
	base-2 log (see Appendix \ref{ecn-fallbacktr_ilog}) and \texttt{V0} is a reference mean-deviation parameter (default
	\(750\,\mu\)s). It is increasingly hard to achieve smaller deviations, so it is
	necessary to use a log function in order to ensure that a mean deviation of,
	say, \(23\,\mu\)s moves the classic ECN variable as much downwards as a mean
	deviation of 24\,ms moves it upwards (respectively 32 times smaller and 32 times
	larger than \texttt{V0} = \(750\,\mu\)s).
	
	\item[Likely Mean Queue Depth, \texttt{d} (\S\,\ref{ecn-fallbacktr_RTT_min}:]
	The metric \texttt{lg(max(d/D0), 1)} uses the log of the ratio of \texttt{d}
	over the reference queue depth \texttt{D0} for the same reason as the previous
	bullet. However, the max() with respect to 1 ensures it ignores queue
	depths below the threshold, which could be suspect, as explained in
	\S\,\ref{ecn-fallbacktr_RTT_min}.
	
	\item[Self-Limitation, \texttt{s} (\S\,\ref{ecn-fallbacktr_self-limited}):]
	Here the fraction of the RTT that was self-limited can be used directly. 
\end{description}

The last two variables can each only push \texttt{classic\_ecn} in one
direction (see \autoref{fig:ecn-fallbacktr_metric-push-pull}). Mean queue depth can only push it upwards (more classic), while
self-limitation can only push it downwards (less classic). If mean queue depth
is below the reference queue depth \texttt{D0}, or there is no self-limitation
in a round, the \texttt{classic\_ecn} indicator remains unchanged.

If, on balance, the calculations to detect classic ECN AQM are positive, 
delta\_ increases \texttt{classic\_ecn} towards its maximum
(\texttt{C\_STICKY}). But if they are negative, delta\_ decreases \texttt{classic\_ecn} 
towards its minimum (\texttt{-L\_STICKY}) where further calculations
will be suppressed, at least until calculations are reawakened by the next CE
mark.

During an idle period, \texttt{classic\_ecn} is exponentially reduced by
default to 1/2 of its previous value on every expiry of the idle timer, but
only if it is positive. Thus, while idling, a connection that had detected a
classic ECN AQM will gradually drift to the L4S end of the transition, but
towards the cusp of the transition. Then, if it continues to detect a classic
ECN AQM once it restarts, It will immediately transition to classic ECN mode
again, while it is restarting.

% ----------------------------------------------------------------
\subsection{RTT Smoothing}\label{ecn-fallbacktr_srtt}

The smoothed RTT and the mean deviation from that smoothed RTT are the 
primary metrics used by the passive classic ECN AQM detection algorithm. They 
both use exponentially weighted moving averages. 

All implementations of TCP already maintain two such variables; the
EWMA of the RTT and an EWMA of the mean deviation of RTT samples from
that smoothed RTT. In Linux, they are called \texttt{srtt} and
\texttt{mdev} and both are updated on every ACK. It was originally hoped
to reuse these
variables for classic AQM detection. However, they are unsuitable for two
reasons:

\begin{itemize}
	\item TCP maintains \texttt{srtt} and \texttt{mdev} for calculation of
	its retransmission time out (RTO). For this it needs a worst-case measure
	of the duration between sending a packet and seeing an ACK. So, whenever
	TCP receives an ACK, it measures the RTT for RTO purposes (\texttt{mrtt})
	from when it sent the \emph{oldest} newly acknowledged packet. This
	includes the duration of the delay introduced by the receiver's delayed
	ACK mechanism, making these metrics unusable as a smoothed measure of the
	actual RTT. The presumption that RTT is used for RTO calculation is also
	built into the time the receiver is expected to stamp into TCP's
	time-stamp option, making timestamps unusable for measuring actual round
	trip delay as well.
	
	\item In Linux, TCP smooths these variables (\texttt{srtt} and \texttt{mdev}) over very 
	few ACKs (8 and 4 respectively) whereas, for classic ECN AQM detection,
	ideally RTT needs to be smoothed over at least one sawtooth of the flow's own
	window, in order to pick up the full depth and variability of the bottleneck
	queue.
\end{itemize}

Terminology: After \(g\) iterations, a step change in an EWMA's input
will have changed the moving average by about 63\% of the step, or
precisely \(1-1/e\), where \(e\) is the base of the natural logarithm.
This is what the phrase smoothed `over' a certain number of iterations
means. It is another way of saying that the smoothing gain of the EWMA is
\(1/g\). For instance, saying that TCP smooths \texttt{srtt} over 8 ACKs,
is alternative way of saying the smoothing gain is 1/8. The smoothing
gain of an EWMA is the fraction of each newly measured value that is
added to the average on every update (and the fraction of the old average
that is subtracted).

In the original discussion of RTO estimation in Jacobson and
Karels~\cite[Appx A]{Jacobson88b:Cong_avoid_ctrl} the EWMAs for
\texttt{srtt} and \texttt{mdev} were recommended to be smoothed over
respectively a little greater and a little less than the congestion
window, measured in segments. However, the Linux code has never related
these parameters to \texttt{cwnd} and they remain hard coded as they were
when typical values of \texttt{cwnd} were hundreds of times lower than
they are today.\footnote{Linux TCP uses a third gain value of 1/32 in
the case where \texttt{mrtt} is less than the smoothed average AND its
distance from the average has increased. A comment in the code points to
the Eifel algorithm as a possible rationale, but another comment
sarcastically says that the code implements the opposite of what was
intended, without saying why it has not been fixed.}

Linux already maintains a more precise RTT metric on each ACK. It stores
the times at which it sends every packet and it measures a variable we
shall call \texttt{acc\_mrtt} from the time at which it sent the packet that
elicited the ACK to when it receives the ACK.

It is not costly to maintain an extra pair of EWMAs based on this more
precise \texttt{acc\_mrtt} variable. However, it is necessary to detect queue variations over the timescale of TCP's sawteeth, which requires smoothing over a very large number of ACKs; far more than the 4 or 8 currently used in Linux and other OSs for RTO calculation.

\autoref{ecn-fallbacktr_srtt_adapt} addresses the question of how many ACKs to smooth over. The theoretical number of ACKs in a Classic sawtooth is \((\texttt{ssthresh})^2\). However, the appendix explains that this can be considered as a rare upper limit. In practice the sawteeth of Classic congestion controls rarely reach this size, especially in the presence of other traffic, such as short flows.

In experiments with a range of link rates between 4\,Mb/s and 200\,Mb/s
and RTTs between 5\,ms and 100\,ms, the formula that
resulted in a good compromise between precision and speed of response
was: \[\mathtt{fbk\_g\_srtt} = 2 * (\mathtt{ssthresh})^{3/2}.\]

And the mean deviation is smoothed twice as slowly as the RTT
itself, i.e.: \[\mathtt{fbk\_g\_mdev} = \mathtt{fbk\_g\_srtt} * 2.\]
\bob{Currently, the Linux code uses g\_mdev = g\_srtt * 2, but I would like to try g\_mdev = g\_srtt and even g\_mdev = g\_srtt / 2}

% ----------------------------------------------------------------
\subsection{RTT Smoothing Pseudocode}\label{ecn-fallbacktr_pseudocode_srtt}

\begin{verbatim}
// fbk_g_srtt = U2 * (ssthresh)^(U1)
#define U1 (3/2)
#define U2 2
#define FBK_G_RATIO 2    // fbk_g_mdev = fbk_g_srtt *  FBK_G_RATIO

/* Stored variables */
fbk_srtt;                // Smoothed RTT
fbk_mdev;                // Mean deviation
fbk_g_srtt;              // srtt gain (initialized dependent on ssthresh)
fbk_g_mdev;              // mdev gain (initialized dependent on ssthresh)

/* Temporary variables */
u32 acc_mrtt;            // Measured RTT from newest unack'd packet
s64 error_;

{   // At start of connection, either after first RTT measurement or from dst cache
    fbk_srtt = acc_mrtt;
    fbk_mdev = 1;        // No need for conservative init, unlike for RTO
}

{   // Per ACK
    // Update EWMAs
    error_ = acc_mrtt - fbk_srtt;
    fbk_srtt += error_  /  fbk_g_srtt;
    fbk_mdev += (abs(error_) - fbk_mdev) / fbk_g_mdev;
}

{   // Per ssthresh change, including when initialized
    fbk_g_srtt = U2 * ssthresh^U1
    fbk_g_mdev = fbk_g_srtt * FBK_G_RATIO
}
\end{verbatim}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\paragraph{RTT Smoothing Pseudocode Walk-Through}

The EWMAs in the `Per ACK' block are straightforward. The mean deviation
is defined as the EWMA of the non-negative error, so it is calculated
using the \texttt{abs()} function, which returns the absolute
(non-negative) value of its argument.

In the `Per-\texttt{ssthresh} change' block, the gains used for the EWMA
are adjusted to maintain their relationship with \texttt{ssthresh} as
just explained in \S\,\ref{ecn-fallbacktr_srtt}.

Appendix \ref{ecn-fallbacktr_srtt_adapt} gives more detailed pseudocode
based on that above, to address the questions of EWMA precision and
upscaling when using integer arithmetic in the kernel.

% ----------------------------------------------------------------
\subsection{Questioning Assumptions used for Passive Detection}\label{ecn-fallbacktr_detection_open}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\subsubsection{Clocking Interval for \texttt{classic\_ecn}}\label{ecn-fallbacktr_clocking}

So far, it has been assumed that \texttt{classic\_ecn} should be recalculated
once per RTT, for all the detection metrics except idling time. The question
of which event is appropriate to update this variable needs to be addressed explicitly.

Four potential intervals on which to clock changes are:
\begin{itemize}
	\item round trips
	\item absolute time intervals
	\item after a certain amount of sent packets, or even sent bytes
	\item after a certain amount of feedback (ACK counting).
\end{itemize}

\begin{description}
	\item[Stabilization and convergence:] It makes sense to count how long to wait
	for a connection to stabilize and converge in round trips, because each flow
	adjusts iteratively on a round trip timescale.
	
	\item[RTT variability:] There is an argument that changes to
	\texttt{classic\_ecn} due to RTT variability should be clocked on a count of
	the ACKs received, e.g. every 32 or every 64 ACKs. This is because the
	precision of round trip smoothing and measurements of mean deviation depends
	on how many ACKs have contributed to the average. However, the variability of
	the queue itself alters dependent on evolution of each flow's congestion
	window, which adjusts on a round trip timescale. Therefore dependence of the
	value of \texttt{classic\_ecn} on RTT variability metrics should be clocked
	against round trips.
	
	\item[Self-limitation:] Self-limitation is measured as a proportion, so it
	does not matter whether it is a proportion of a round, a proportion of a
	certain time period, or a proportion of any other metric. Given other metrics
	should be clocked on round trips, it makes sense to clock self-limitation
	calculations on the same events.
	
	\item[Idling:] There is no basis to argue that changes to \texttt{classic\_ecn}
	due to idling should be clocked on any particular metric. Nonetheless, the
	only metric that continues to clock during an idle period is time, so this is
	the only practical metric to use.
\end{description}

Counting either in sent packets or absolute time would be easy to implement,
but neither seem to have any logical backing, for any of the metrics.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\subsubsection{Clocking Interval for RTT EWMAs}\label{ecn-fallbacktr_clocking_ewmas}

\autoref{ecn-fallbacktr_srtt_adapt} addresses the question of how many
ACKs to smooth the RTT EWMAs over. However, it does not question whether
these EWMAs should be or need to be updated on every ACK, particularly
given the gain has to be so tiny, which implies that less frequent
updates with a larger gain would be no less precise but less costly in
processing terms.

It would be possible to measure the RTT of only a sample of ACKs, and
increase the gain accordingly. However, this would involve extra
complexity, and the actual additional processing cost is only 4 adds, 2
bit-shifts and a compare per ACK (see Appendix
\ref{ecn-fallbacktr_srtt_adapt}), which is not particularly problematic.
Sampling has its own complexity because the sampling ratio would have to
be adaptive, in order that it could revert to 100\% when the number of
ACKs per round trip was small. It would also require an extra state
variable to be maintained per ACK, in order to record when the next
sample was due.

Therefore, on balance it has been decided to maintain the EWMAs of RTT
and mean deviation per ACK. But there is nothing to stop other
implementers using sampling.

% ----------------------------------------------------------------
\subsection{Parameter settings}\label{ecn-fallbacktr_detection_params}

The parameters currently given in the passive detection pseudocode
(\S\S\,\ref{ecn-fallbacktr_passive_detection} \&
\ref{ecn-fallbacktr_pseudocode_srtt}) are heuristics arrived at as a
result of a large number of calibration experiments over a testbed with
link rates of 4--200\,Mb/s and RTTs of 5--100\,ms. Higher link rates
could have been included, but the most challenging part of the range is
the low end. \S\,\ref{ecn-fallbacktr_evaluation} summarizes the results
of evaluations using the parameters resulting from these calibration
experiments.

The current set of parameter values has solely been informed by
experiments with CoDel~\cite{Nichols18:codel_RFC} as the Classic AQM 
and DualPI2~\cite{Briscoe15e:DualQ-Coupled-AQM_ID} as the L4S AQM
(both with default settings). CoDel was chosen given it is a good 
candidate for the worst-case for the fall-back algorithm to detect, for two 
reasons:
\begin{itemize}
\item CoDel's default setting of \texttt{target} is very low (5\,ms). This pegs its
queue delay low relative to other Classic AQMs and tends to cause
under-utilization for all but the lowest RTTs. Both these factors lead to
very little, if any, queuing delay. This makes CoDel more difficult to
distinguish from an L4S AQM, and therefore likely to be a worst-case from
a parameter-setting viewpoint.
\item CoDel's design is highly specific to 
Classic congestion controls, probably exhibiting the slowest response of 
all AQMs to the high levels of ECN signalling induced by scalable 
congestion controls~\cite{Palmei19:Cobalt}. 
\end{itemize}
As the algorithm is evaluated with other
AQMs, it may be necessary to tune the parameters further.
