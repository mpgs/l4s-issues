% !TeX root = ecn-fallback_tr.tex
% ================================================================
\section{Core or Peering Link with Shared-Queue Classic ECN AQM}\label{ecn-fallbacktr_common}

First, for brevity, we will use the term `common link' for either a core link or
a peering link. This appendix is about the possibility that such a common link 
could use a shared-queue Classic ECN AQM, and that it could become a bottleneck.

Initially, let us assume an ideal situation in which end systems all ensure equal flow rates at a
bottleneck and let us define the equal division of a bottleneck's capacity
among all flows bottlenecked there as the 'equitable rate' for that
bottleneck.

Usually networks are designed so that flows bottleneck in access links. 
If the number of flows converging into a common link grows, 
even though they are all bottlenecked elsewhere, there will 
come a point where the sum of all the flows feeding traffic into the common link exceeds its
capacity. If the number of flows continues to rise, the equitable rate for the
common bottleneck will continue to reduce. As the equitable rate reduces below the
highest capacity access links, the bottleneck for any lone flow in each of those
access links will move to the common link.

Let us now imagine that the equitable rate has just reduced to 50\% of the
capacity of the fastest access link feeding the common bottleneck. If it
contains one flow, that will now be running at half the access rate. If it
contains two flows, the bottleneck for them will start to move to the common
link as well.

Now let's change the scenario by replacing some classic sources with L4S. At
the common bottleneck (still assuming a Classic ECN AQM), there will be little variability in the queue because
of the high degree of multiplexing. So as the bottleneck moves there, L4S
flows will not detect a classic ECN bottleneck and they will yield less than
any classic flows. Classic flows will end up below the equitable rate and L4S
flows above it. However, as L4S flows increase, they will bottleneck in their
own access link again, which will naturally limit the inequality to \(
100\%/50\% = 2\times\).

Of course, serious anomalies might concentrate so much load at a common link
that the equitable rate reduces to less than 50\% of the fastest access, say
\(x\%\). Then the worst inequality would be \(100/x\times\). But it is
extremely rare for an anomaly to even reduce \(x\) to 50\%. In robustly
designed networks, even during an anomaly, \(x\) will only just dip below
100\%, e.g. 95\%. Then the worst inequality due to classic ECN fall-back not
detecting a classic ECN AQM in a highly multiplexed common link would be
\(100/95 = 1.05\times\).

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{dual-anomaly}
\caption{Example before (left) and after (right) a dual anomaly that shifts 
congestion to a core link}\label{fig:ecn-fallbacktr_dual-anomaly}
\end{figure}

\paragraph{Example:} Starting with the network as designed on the left of
\autoref{fig:ecn-fallbacktr_dual-anomaly}, the 8 circles in the centre
are core routers. The rectangles connected around them are broadband
network gateways (BNGs), each dual-homed to two core routers, and each
providing Internet access to 8192 customer access links. For ease of
explanation, the access links all have the same 120\,Mb/s capacity.

The network is designed on the assumption of 2.5\% access network
utilization outwards, i.e.\ 2.5\% of access links are fully utilized at
any one time (or proportionately more than 2.5\% are partially utilized).
Thus, each BNG needs 2.5\%*8192*120\,Mb/s = 25\,Gb/s. The core links are
40\,Gb/s. Assuming reasonably equal load balancing in the core (e.g.\
randomized equal cost multipath routing), this implies each core link is
designed to be utilized at roughly \(25/(40*2) = 31\%\).

The core's design utilization is under 50\%, which is common for
dual-homed cores because, if one core router or link fails, the remaining
core capacity will still be sufficient. This keeps the BNGs as the
bottlenecks (then more costly per-customer schedulers only need to be
deployed at one node on each path).

Now let's consider two anomalies at once, as on the right hand side of
\autoref{fig:ecn-fallbacktr_dual-anomaly}. As well as a core node
failure, there happens to be unusually high utilization on one BNG,
averaging 5\%; double the design utilization. This BNG needs 50\,Gb/s of
core capacity, but it only has one 40\,Gb/s link left to connect it to
the core. This moves the bottleneck from the BNG to the interface from
the core router into the core link shown coloured red. However core
routers do not provide per-customer scheduling, so the capacity share
that each customer gets now depends on their end-system congestion
control algorithms (e.g.\ TCP), not the network's schedulers.

If every one of the 5\% of customers actively downloading at any one time
were using just a single Classic flow with perfect capacity sharing, each
access link would be utilized at \(40\,Gb/s / (5\%*8192) / 120\,Mb/s =
81\%\). Therefore, if one customer was to switch to an L4S congestion
control instead of Classic, as soon as it used more than \(1/0.81 =
1.23\times\) the Classic equitable share, its bottleneck would shift back
to its own access link, thus limiting any further advantage.

This would be no different from the similar common link bottleneck
scenario where all customers except one open a single flow. The one
opening more flows can only get \(1.23\times\) more capacity than
everyone else before their bottleneck shifts back to their own access
link.

\paragraph{In summary:} Generally, public Internet access networks do not
rely on end-system congestion controls to equitably share out capacity
between their customers. They rely on per-customer schedulers, but they
usually only deploy these at one node on the path, which they design to
be the bottleneck. A core or peering link can become the bottleneck under
anomalous conditions, so that capacity sharing does temporarily rely on
end-system congestion controls. However, if it does, any individual
congestion control only has limited scope to take advantage of the
situation before it becomes bottlenecked again within its own access
link.





