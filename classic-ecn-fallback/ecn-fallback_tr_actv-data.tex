% !TeX root = ecn-fallback_tr.tex
% ================================================================
\section{Out-of-Band Active Detection `of Classic ECN AQMs}\label{ecn-fallbacktr_OOB_active}

Out-of-band testing presents few constraints on the traffic patterns that can be
used. So it is possible to use unsubtle approaches like running two flows in
parallel, which is described here.

A server could be set up with ECN enabled so that, when a test client accesses
it, it serves a script that gets the client to open two parallel long-running
flows. Then it could serve one with a Classic CC (that sets ECT(0)) and one with
a scaleable CC (that sets ECT(1)).

If neither flow induces any ECN marks, it can be presumed the path does not
contain a Classic ECN AQM.

If either flow induces some ECN marks, the server could measure the relative
flow rates and round trip times of the two flows.
\autoref{tab:ecn-fallbacktr_OOB_active} shows the AQM that can be inferred for
various cases.

\begin{table}[h]
\centering
\begin{tabular}{|c|c|l|}
	\hline
	Rate          & RTT & Inferred AQM \\
	\hline
%	\(L=C\)        & \(L=C\) & Tail-drop or Non-ECN AQM (FQ or FIFO) \\
	\(L>C\)        & \(L=C\) & Classic ECN AQM (FIFO)\\
	\(L=C\)        & \(L=C\) & Classic ECN AQM (FQ)\\
	\(L=C\)        & \(L<C\) & FQ-L4S AQM\\
	\(L\approx C\) & \(L<C\) & DualQ Coupled AQM\\
	\hline
\end{tabular}
\caption{Out-of-band testing with two parallel flows, showing those cases with
	some ECN marks apparent. L=L4S, C=Classic.}\label{tab:ecn-fallbacktr_OOB_active}
\end{table}

The power of this approach is that is can identify whether a Classic ECN AQM is
in a FIFO (which is the type being sought), or in an FQ scheduler (which is
not).

In the case of the DualQ Coupled AQM, the relative rates will depend on the
configuration, so they are only shown as '\(\approx\)'. Other combinations of
outcomes are theoretically possible, e.g.\ \(L>C\) for RTT, but the test would
have to be abandoned in such cases, because no known AQM would cause such an
outcome.

% ================================================================
\section{In-Band Active Detection of Classic ECN AQMs}\label{ecn-fallbacktr_active}

% ----------------------------------------------------------------
\subsection{In-Band Active Detection: ACK Problems in TCP}\label{ecn-fallbacktr_active_problem}

When active detection is used in-band, by definition it alters the traffic. So
care has to be taken not to intrude too much into live sessions.

The most likely testing pattern would be to start with passive testing, then
only introduce a little active testing before changing the CC behaviour,
particularly if the results were not clear cut. This is the approach expected
for Solution No.1 (\S\,\ref{ecn-fallbacktr_active_solution1}). Alternatively,
active testing could be used on a small sample of flows, which is the
most likely approach to be taken for Solution No.2
(\S\,\ref{ecn-fallbacktr_active_solution2}).

We start out on the assumption that an L4S sender will set some packets within a
single flow to ECT(0) even though it should set them to ECT(1). In Solution No.1
the sender checks for differences in RTT. In Solution No.2 it checks for
differences in marking.

One can imagine a number of na\"ive active measures that a sender could take:
\begin{itemize}
    \item It could duplicate a small proportion of ECT1 packets and set them as ECT0.

    \item it could set a small proportion of packets to ECT0 instead of ECT1;
\end{itemize}

We shall call these 'ECT tracer' packets,
because they trace whether the ECT field causes a packet to be classified into
a different queue. However, if TCP is being used, and if the receiver was using delayed ACKs (most do),
it would confound these na\"ive approaches:
\begin{itemize}
	\item in the first case, even if both duplicates were acknowledged (the first
	to arrive might not be), the sender would not be able to tell from the
	acknowledgement(s) which duplicate had arrived first\footnote{Unless AccECN
	TCP feedback with the TCP Option was implemented and it successfully traversed
	the path, but that is too unlikely to rely on}.
	    
	\item In the second case, some ECT0 packets would not trigger an ACK so their
	delay could not be measured. Also, if the bottleneck were an L4S DualQ Coupled
	AQM, any queuing delay suffered by the ECT0 packets would hold back the
	connection, and some might be delayed enough relative to ECT1 packets to make
	TCP believe they had been lost, causing the sender to spuriously retransmit
	and spuriously reduce its congestion window.
\end{itemize}

% ----------------------------------------------------------------
\subsection{In-Band Active Detection: Solution No.\,1}\label{ecn-fallbacktr_active_solution1}
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\subsubsection{Active Solution No.\,1: Approach}\label{ecn-fallbacktr_active_solution1_approach}

A better strategy would be:
\begin{itemize}
	\item for the sender to make the receiver override its delayed ACK mechanism
	by ensuring that at least part of both tracer packets duplicates bytes already
	sent. This is because standard TCP congestion control
	\cite{IETF_RFC5681:TCP_algorithms, IETF_RFC2581:TCP_algorithms} recommends
	that a receiver sends an immediate ACK in response to duplicate data to
	expedite the fast retransmit process, and this recommendation has stood since
	the first Internet host requirements in 1989~\cite{IETF_RFC1122:HostReqs}.
		
	\item for either tracer packet to push forward the acknowledgement counter, so
	that the sender can tell which probably arrived first (there can be no
	certainty, because ACKs can be reordered). \end{itemize}

The best sender-only strategy so far conceived would be as follows (also
illustrated in \autoref{fig:ecn-fallbacktr_cc-changeover}):
\begin{enumerate}
	\item If, during passive testing, the \texttt{classic\_ecn} indicator is approaching the transition
	range from below, i.e.\ negative but close to zero, for a small proportion of
	segments send instead the following three smaller packets, all back-to-back:
    \begin{itemize}
		\item a larger front segment marked ECT1;
		 
		\item a smaller middle segment marked ECT0, duplicating at least the last 2\,B
		of the first segment;
		 
		\item a rear segment marked ECT1 of the same size as the second but only
		duplicating the last byte of the first segment;
    \end{itemize}
	\item If the ACK for the middle tracer arrives after that for the rear tracer,
	the AQM is likely to be L4S (unless some other mechanism happens to have
	coincidentally re-ordered the packet stream at this point);
\end{enumerate}

\begin{figure}
  \centering
  \includegraphics[width=0.49\linewidth]{ect-classifier-tracer}
  \caption{Tracer packets to detect separate treatment of ECT1 packets}\label{fig:ecn-fallbacktr_ect-classifier-tracer}
\end{figure}

Note that the ECT0-marked packet only includes redundant bytes, so if it is
delayed (or dropped) by a classic queue, it does not degrade the L4S service.

The combined size of all three packets should be no greater than 1\,MTU so
that, if packet pacing is enabled, all three packets will remain back-to-back
without having to alter pacing (also the two back-to-back ECT1 packets will
cause no more of a burst in an L4S queue than a single packet would).

The front packet is larger to reduce the risk that detection of L4S AQMs will
sometimes fail. Being larger, it is more likely to still be dequeuing when the
rear packet arrives at the bottleneck. Otherwise, if there was a DualQ Coupled
AQM at the bottleneck, and if there was no other classic traffic queued ahead
of the middle tracer, it could start dequeuing after the front packet had
dequeued, but before the rear tracer arrived.

Nonetheless, in order to minimize the possibility that the small tracer
packets are treated differently by middleboxes, they should be larger than the
size \(S\) of the largest packet that might be considered 'small' by common
acceleration devices (\(S = 98\)\,B would probably be sufficient).

%This probing transmits more bytes for selected packets, so it might be necessary to only select a proportion, \(r\). Also, only a few segments would need to be split for active testing---only once passive measurements had raised suspicions of a classic AQM, but before committing to any change in the L4S behaviour. 

%As an example of the overhead, let's assume the MTU, \(M = 1500\)\,B; the size of the TCP/IP packet headers in use is \(H = 40\)\,B; the sizes of the last 2 segments are \(L = 98\)\,B. Then the size of the first segment will be \(M-3*H-2*L = 1180\)\,B.\footnote{Even if the full TCP option space is used so that H=80\,B, the first segment will be 1064\,B, which is still more than a decimal order of magnitude greater than either of the last segments} The efficiency relative to sending all normal full MTU packet will be: \(1-r*(2*H+L+1)/(M-H) \approx 1 - r*12\%\). For instance, if \(r\) is 1 in 8, then efficiency \(\approx 98.4\%\). 

%If 100\% of packets were broken into three for a short while, efficiency would be 88\%, which might be acceptable.

\bob{ToDo: Write up a symmetric facility at the classic end of the spectrum.
And write-up pseudocode in the following section, including a way to take
multiple active measurements and act on their combined outcome.}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\subsubsection{Active Solution No\.1: Pseudocode}\label{ecn-fallbacktr_active_solution1_pseudocode}

The following pseudocode implements the active detection ideas in
\S\,\ref{ecn-fallbacktr_active_solution1_approach}. It uses some of the macros and variables
defined in the passive detection pseudocode above.

\begin{verbatim}
// Parameters
#define TRACER_NUM 4    // Number of sets of 3 active tracers to send
#define REAR_SIZE 98    // Min size of middle and rear tracers [B]

ect_tracers = 0;  // Unsigned int storing remaining tracers (-ve means disarm sending)
tracer_nxt = 0;   // Point in the sequence space after the most recently sent tracer
                  // special (tracer_nxt == 0) disables checking for tracer ACKs

// Functions
send_tracer(start, size, ecn);    // Sends ECT tracer seg from 'start' in send buffer

// The following statements are intended to be inserted at the stated events

{   \\ Per RTT
    if (classic_ecn >= -L_STICKY/TRACER_NUM && !ect_tracers) {
        ect_tracers = TRACER_NUM;
    }

    if (ect_tracers < 0)    // The tracer armed 1RTT ago has been sent
        ect_tracers *= -1;    // Arm sending of the next tracer
}
    
{   // Prior to sending a packet
    if (ect_tracers > 0 && snd_q >= smss) {
        front_size = smss - 2 * (sizeof_tcp_ip_headers() + REAR_SIZE);
        send_tracer(snd.nxt,                 front_size, ECT1);    // Front tracer
        send_tracer(snd.nxt - 2,             REAR_SIZE,  ECT0);    // Middle tracer
        send_tracer(snd.nxt - REAR_SIZE + 1, REAR_SIZE,  ECT1);    // Rear tracer
        tracer_nxt = snd.nxt;
        if (--ect_tracers) {
            ect_tracers *= -1;    // Negate to disarm sending of the next tracer
        } elif (classic_ecn >= -L_STICKY/TRACER_NUM) {
           ect_tracers -= TRACER_NUM + 1;    // Suppress further tracers
        }
    }
}

{   // On receipt of seg (pure ACK or data) 
    if (tracer_nxt) {
        if (rcv.nxt == tracer_nxt && seg.sack == tracer_nxt - 1)
            // middle arrived after rear, so probably L4S bottleneck
            classic_ecn = max(classic_ecn - L_STICKY/TRACER_NUM, -L_STICKY);
        if (ect_tracers == -TRACER_NUM - 1)    // Further ECT tracers have been suppressed
            tracer_nxt = FALSE;    // Suppress further ACK checking
    }
}
\end{verbatim}

\paragraph{Interaction between Active Testing and the \texttt{classic\_ecn} Indicator:}

Greater RTT variability during passive testing might imply either a classic bottleneck or an L4S
bottleneck combined with variability from another link (e.g.\ non-L4S WiFi).
Whereas low variability is more likely to imply an L4S bottleneck. Therefore
if the result of an active test is L4S, it pushes the \texttt{classic\_ecn}
indicator towards the L4S end, counteracting the pposite trend due to
variability. Whereas if the result of an active test is classic, it does not
need to alter \texttt{classic\_ecn}; it can leave variability to do that.

Active detection is more decisive, but it alters the normal transmission
pattern. So to avoid unnecessarily altering the sending pattern, passive
measurement alone is used first to determine whether active measurement is
worthwhile.

If active measurement proves necessary, the plan then is to send a small
number (default TRACER\_NUM = 4) of sets of three tracer packets. If any set
of tracers detects that an L4S AQM is likely, it moves the
\texttt{classic\_ecn} indicator towards the L4S end of the spectrum by an
amount \texttt{L\_STICKY/TRACER\_NUM}.

Thus if all 4 tests detect L4S, \texttt{classic\_ecn} reduces by
\texttt{L\_STICKY}. The tests start at \texttt{classic\_ecn >= -L\_STICKY/4},
so if all 4 tests detect L4S, it will return to its floor value of
\texttt{-L\_STICKY}, and the CC will never have behaved as anything other than
pure L4S. Bear in mind that the \texttt{classic\_ecn} indicator will still be
altered by the passive detection algorithm as well.

If, on the other hand, no set of tracers detects L4S, the active tests will
not alter the \texttt{classic\_ecn} indicator at all. Then, if the bottleneck
is classic, continuing passive tests will detect the higher RTT variability
and continue to push the \texttt{classic\_ecn} indicator towards the classic
end of the spectrum.

Between these two extremes, if not all the active tests detect L4S, the
\texttt{classic\_ecn} indicator will be pushed down less and stop short of its
floor. Then if RTT variability continues, passive detection will more rapidly
return it to the \texttt{-L\_STICKY/4} threshold where active tests resume.

\paragraph{State Variables}

To detect which tracer packet arrived first, it is necessary to store an
indication of which feedback to check. Therefore no more than one set of
tracers is sent per round trip, to minimize the per-connection state needed.
This also spaces out the tracer tests, so that the small amount of redundant
data each one sends hardly causes any inefficiency\footnote{With typical MTU
and header sizes, a set of 3 tracer packets consumes 1\,MTU, but sends about
12\% less TCP data that would normally be in a full MTU. If there were say 16
packets per round, this inefficiency would be reduced to 12\%/16 = 0.75\%}.

Two additional state variables are needed for each connection:
\begin{itemize}
	\item \texttt{ect\_tracers}: This state variable stores the number of ECT
	tracers outstanding. Zero is not really a special value; it just has the
	expected meaning---that no tracers are outstanding.
	
	\item \texttt{tracer\_nxt}: After a set of three tracer packets have been
	sent, \texttt{tracer\_nxt} stores the next byte in the sequence space. Then
	later the matching ACKs for the tracers can be found. If the ACK never
	arrives, there is just no outcome to the test.
\end{itemize}

Negative values of \texttt{ect\_tracers} are special; they store the number of
outstanding sets of tracers but disarm them for a round trip (so that the
feedback from the last one has time to return).\bob{There could be a race condition
here, where the variable will be needed for the next tracer before it has been used
to pick up the feedback from the last one.}

The negative value of \texttt{ect\_tracers} one lower than
\texttt{-TRACER\_NUM} (-5 by default) is a further special value that
suppresses all further tracers.

Tracers are not suppressed as long as the outcome after 4 tracers has reduced
the \texttt{classic\_ecn} indicator below the threshold at which active tests
are triggered (\texttt{-L\_STICKY/4}). Then, if the indicator rises to this
threshold again, another set of tracers can be triggered. But, if the
indicator has not reduced after the 4 tracer tests (i.e.\ all 4 tracer tests
pass without reordering), all further active tests are suppressed so that
continuing passive measurements are allowed to push the indicator upwards
towards the classic ceiling (causing the CC to transition to classic
behaviour).

If RTT variability reduces (e.g.\ because the bottleneck moves from a classic
to an L4S AQM) such that the passive tests on their own pull the indicator
down to the L4S floor, active tests suppression is removed by setting
\texttt{ect\_tracers = 0}.

\paragraph{Per Packet Processing Efficiency}

The special values of the variables \texttt{ect\_tracers} and
\texttt{tracer\_nxt} are used to suppress the more complex conditions that
would otherwise have to be checked per packet, respectively: whether each
packet to be sent should be replaced by a set of tracers; and whether each ACK
is feedback from a tracer.

For efficient implementation, rather than checking a flag variable on millions
of packets just to send or receive a few packets differently, it might be
better to somehow suppress regular packet sending, send the required number of
tracer packets manually, then resume sending. This will need to be
investigated during implementation.

% ----------------------------------------------------------------
\subsection{In-Band Active Detection: Solution No.\,2}\label{ecn-fallbacktr_active_solution2}
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\subsubsection{Active Solution No.\,2: Approach}\label{ecn-fallbacktr_active_solution2_approach}

To date, the draft text of the experimental IETF RFC to specify the L4S
markings says:
\begin{verbatim}
   For backward compatibility in uncontrolled environments, a network
   node that implements the L4S treatment MUST also implement an AQM
   treatment for the Classic service as defined in Section 1.2.  This
   Classic AQM treatment need not mark ECT(0) packets, but if it does,
   it will do so under the same conditions as it would drop Not-ECT
   packets [RFC3168].
\end{verbatim}

It has been suggested that this could be amended to preclude any Classic AQM
from marking ECT(0) packets if it is coupled with an L4S AQM. Then, an L4S
source could detect a Classic ECN AQM by probing with ECT(0) packets to see if
they were marked. In the following, this will be termed \textbf{exclusive}
marking.

With a flow-queuing (FQ) scheduler, an L4S and a Classic AQM can both be applied
within each queue~\cite[\S\,5.2.7]{Hoeiland18:fq-codel_RFC} by applying
immediate ECT(1) marking at a shallower threshold. It has also been suggested
that it would not be necessary for the whole FQ system to choose between L4S and
Classic ECN. Instead, within each per-flow queue (FQ), the queue could continue
to support Classic ECT(0) marking, except it could be required not to mark
ECT(0) packets if the same queue had `recently' seen an ECT(1) marking. In the
following, this will be termed \textbf{FQ-exclusive} marking.

`Recently' could mean in the lifetime of the per-flow bucket, or after a
timeout. If the timeout option were chosen, it would require at least a 2-bit
counter per queue. The timeout could be implemented by initializing each counter
to zero, then setting it to 3 on every ECT(1) packet. Then a timer shared over
all queues could decrement all the counters at a regular interval, \(T\). ECT(0)
marking would be disabled in any queue with a non-zero counter, so the timeout
would last for at least \(2T\) and at most \(3T\).

The whole `Solution No.2' approach would still work even if some FQ AQMs
supported both L4S and Classic AQMs but did not implement FQ-exclusive marking
(important given such AQMs are already deployed). This is because the queue for
a flow with a decent proportion of ECT(1) packets would tend to sit below the
shallow ECT(1) threshold and never reach the deeper point where ECT(0) marking
started.

A notable benefit of the exclusive marking idea is that it only needs to be
temporary, for instance during the early stages of the experimental phase of
L4S. If it proves not to be useful, perhaps because some better approach is
developed, the standards can be updated and Classic ECN AQMs can be allowed to
mark ECT0 packets alongside L4S ECT1 marking.

The following two test strategies have been proposed to exploit exclusive
marking, if it were standardized: 
\begin{description} 
	\item[ECT0 probes:] One approach would be to intersperse small ECT(0) probe
	packets within an L4S ECT(1) flow, while attempting to induce enough congestion
	for some of these probe packets to be likely to be dropped or marked. Then if
	any ECT(0) packets were marked it would be highly likely that there was a
	Classic ECN AQM at the bottleneck.

	\item[Late onset ECT1 stripe:] In this approach, a long-running flow would
	start with all ECT0 packets. Then after some congestion had been induced (CE
	marking or loss), a small proportion of packets (e.g.\ 5\%) would be marked
	ECT1 while the majority would continue as ECT0.
\end{description}

More details of each strategy including pros and cons are discussed below. It is
uncertain whether either would be considered acceptable for in-band testing;
ECT0 probes might be too slow, and the late onset ECT1 stripe might not be
sufficiently benign. However, other strategies might be developed.

\paragraph{ECT0 Probes:} This approach relies on being able to tell whether
specific packets have been ECN-marked. Therefore, if it were used over a TCP
connection, a technique similar to that in
\S\,\ref{ecn-fallbacktr_active_solution1_approach} would be needed to avoid the
sender being confused by delayed ACKs.

Although a marking on any ECT0 probe would prove that a Classic ECN AQM was
likely, absence of such a marking would not prove absence of a Classic ECN AQM.
If the ratio of ECT0 probes to ECT1 packets was \(1:P\), then the test would
have to continue until the number of ECT1-marked packets had exceeded some
multiple of \(P\), e.g. \(m*P\) (say  \(m=10\)), to improve the chances. This
would only be reliable if the flow and its ECT1 marking was steady and regular.
Actually the number of ECT1 packets would have to exceed \(m*r*P\), where \(r\) is
the ratio of the largest to the smallest packet size used by some AQMs to
bias marking towards larger packets. Although deprecated by IETF RFC 7141,
some common AQMs adopt this practice, for instance DOCSIS
PIE~\cite[\S\,4.6]{White17:PIE_RFC}.

\paragraph{Late onset ECT1 stripe:} This approach would offer typical Classic
queue delay even if the bottleneck AQM supported L4S. So it would have to be
used sparingly on a small sample of flows, perhaps to characterize the paths to
different destinations (or for out-of-band tests). Given L4S connections are
designed to fall back to Classic ECN or no ECN at all where it is only partially
deployed, occasional flows without L4S support should be unremarkable,
particularly during the experimental phase of L4S.

\autoref{tab:ecn-fallbacktr_late_ect1} shows the various patterns of marking
that might result from this test, and what could then be concluded about the
bottleneck in the right-hand column.

\begin{table}[h]
	\newcommand{\drp}{\textrm{drop}}
	\newcommand{\mrk}{\textrm{mark}}
	\centering
	\(
	\begin{array}{|crrc|crrcrrc|l|}
	    \hline
	    \multicolumn{4}{|c|}{\textrm{All ECT0}}
	    		 &\multicolumn{3}{|c}{\textrm{95\% ECT0}}
	    				    &&\multicolumn{3}{c|}{\textrm{5\% ECT1}}
	    									 & \\
	    \cline{2-3}\cline{6-7}\cline{9-10}
	   &\drp&\mrk&&&\drp&\mrk&&\drp&\mrk      && \textrm{The implied type of bottleneck}\\
		\hline
		&>0 &  0 &&& >0 &  0 && >0 &  0       && \textrm{Tail-drop or Non-ECN AQM (FQ or FIFO)}\\
		&   & >0 &&&    & >0 &&    & >0       && \textrm{Classic ECN AQM (FQ or FIFO)}\\
		&   & >0 &&&    & >0 &&    & \approx1 && \textrm{FQ-L4S AQM (non-exclusive)}\\
		&   & >0 &&& >0 &  0 &&    & >0       && \textrm{FQ-exclusive L4S AQM}\\
		&>0 &  0 &&& >0 &  0 &&    & >0       && \textrm{DualQ L4S AQM}\\
		\hline
	\end{array}
	\)
	\caption{Possible patterns of marking resulting from `Late Onset ECT1 Stripe'
	testing. An empty cell means `don't care'.}\label{tab:ecn-fallbacktr_late_ect1}
\end{table}

A run would have to be aborted under the following conditions\footnote{It has
	been checked that these cater for every possible condition of the truth table}:
\begin{itemize}
	\item If no congestion indication (drop or marking) can be induced on an ECT0
	packet, either before ECT1 packets are introduced or after, the run has to time
	out;
	
	\item Whether or not there are congestion indications on any ECT0 packets, a
	run has to eventually time out if there is never a congestion indication on an
	ECT1 packet;
	
	\item If any ECT0 packet has been marked, a run has to eventually time out if
	no ECT1 packet is ever marked;
	
	\item If no ECT0 packet is marked before ECT1 packets are introduced, but an
	ECT0 packet is marked after, the run has to abort.
\end{itemize}
