% !TeX root = ecn-fallback_tr.tex
% ================================================================
\section{Algorithm for Filtering Reroutes out of RTT Metrics}\label{ecn-fallbacktr_alt-srtt}

\begin{figure}
  \centering
  \includegraphics[width=0.7\linewidth]{alt-srtt}
\caption{Example plot of `incorrect' (light red) and `correct' (dark red)
smoothed RTT and their mean deviations after a reroute at ackno
1100}\label{fig:ecn-fallbacktr_reroute}
\end{figure}

If the base RTT suddenly changes, e.g.\ due to a re-route, the smoothed
RTT (\texttt{srtt}) could take a long time to converge on the new value.
In the meantime, the mean deviation (\texttt{mdev}) will expand, not
because there is more queue variability, but because \texttt{srtt} is
`incorrect'. The resultant increase in \texttt{mdev} could cause the
fall-back detection algorithm to incorrectly detect a Classic AQM, even when
it is not and there is very littel queue variability.

The pseudocode below is designed to rapidly lock-on to a better smoothed
RTT following a significant step change in the base RTT. Consequently,
the mean deviation based of this `corrected' smoothed RTT should not
increase when there is a step-change in base RTT.

\autoref{fig:ecn-fallbacktr_reroute} shows an example with both the
uncorrected smoothed RTT in bright red (labelled \texttt{srtt[0]}) and
the corrected smoothed RTT in dark red (labelled \texttt{srtt[1]}).

Numerous data points for the actual RTT measurements are shown as tiny
blue crosses. On closer inspection, a characteristic sawtoothing pattern
can be seen in data points as TCP cycles between capacity-seeking and
relaxing. 
At ackno=1100, a step increase in the sawtoothing pattern can be picked
out, despite the noisy data. For comparison, the bright red plot labelled
\texttt{srtt[0]} is the uncorrected smoothed RTT, which can be seen
slowly converging on the new smoothed average after ackno 1100. In
contrast, the dark red smoothed RTT labelled \texttt{srtt[1]} rapidly
corrects itself by jumping to a `better' value soon after the re-route.

Once the original EWMA converges with the alternative one, the 
algorithm disables calculation of the alternative to save processing,
as can be seen where the dark red plot stops. The algorithm is also useful
at the start of a flow, where it can quickly correct an unfortunate initial
value of the EWMA, as seen on the left of \autoref{fig:ecn-fallbacktr_reroute}.

The black dashed plots labelled \(\mathtt{srtt[0]} \pm
2*\mathtt{mdev[0]}\) illustrate two mean deviations either side of the
uncorrected \texttt{srtt[0]} plot. After the re-route, this mean
deviation can be seen to expand. In contrast, the grey dashed lines
illustrate two mean deviations either side of the corrected
\texttt{srtt[1]} plot (labelled \(\mathtt{srtt[1]} \pm
2*\mathtt{mdev[1]}\) ). It can be seen that \texttt{mdev[1]} hardly
increases at all during the re-route episode.

This is actually a clue to how the algorithm works; whenever there is an
outlier, it starts a new EWMA (\texttt{srtt[1]}) from that outlier and
initializes the new mean deviation (\texttt{mdev[1]}) with a slightly
inflated copy of the original mean deviation (\texttt{mdev[0]}). It only
maintains the new EWMA as long as it results in a tighter mean deviation
than that based off the original EWMA. The inflation factor for the new
mean deviation (\texttt{K1}) is contrived in such a way that the new EWMA
will quickly be rejected if it is not `better'. In particular, even if
the next data point after the outlier is as close as possible to the
first outlier without actually being an outlier (with respect to the
original EWMA), the alternative EWMA will immediately be disabled (and it
will never have been used, because the initial inflated mean deviation
was `worse' than the original). The derivation of \texttt{K1} is left as
an exercise for the reader. \bob{Strictly, a similar exercise using the
lower outlier threshold will need to be done, because the outcome of the
mean deviation approach is asymmetric. Then the greater value of K1
should be used.}

In this way, the algorithm is (usually) able to ignore TCP's sawtooth
jumps, given the mean deviation established over time includes them. By
default, outliers are defined as outside \(\texttt{K2}=2\) mean
deviations.

The pseudocode is given below, followed by a walk-through. It is written
as a general-purpose algorithm that could be used to `correct' any EWMAs
of RTT and its deviation, whether for Classic ECN AQM detection,
retransmit timer calculation or moving averages that are nothing to do
with RTT for that matter. Nonetheless, in the case of Classic ECN AQM
detection, the variables \texttt{srtt[]} and \texttt{mdev[]} are intended
to be equivalent to \texttt{fbk\_srtt} and \texttt{fbk\_mdev} as defined
in pseudocode throughout the rest of this paper (not the \texttt{srtt}
and \texttt{mdev} variables already maintained by Linux for RTO
calculation). Similarly, for Classic ECN AQM detection, the gain
parameters (\texttt{g\_srtt} and \texttt{g\_mdev}) are intended to be
equivalent to \texttt{fbk\_g\_srtt} and \texttt{fbk\_g\_mdev} in the rest
of this paper.

\begin{verbatim}
/* Macros */
#define G1 (1/g_srtt)     // The gain already used to maintain srtt
#define G2 (1/g_mdev)     // The gain already used to maintain mdev
#define K2 2              // Outlier threshold, as multiple of mdev
#define K1 1+G2*(K2*(1-G1)+(K2-1)*(1-G2)-1) // Hysteresis factor (see text later)
// SRTT or MDEV can be used wherever TCP uses srtt or mdev
#define SRTT (srtt[1] && mdev[1] < mdev[0] ? srtt[1] : srtt[0]) 
#define MDEV (srtt[1] && mdev[1] < mdev[0] ? mdev[1] : mdev[0])

/* Definitions of variables and functions
 *	srtt[1] and mdev[1] are candidate alternatives to srtt[0] and mdev[0], 
 *  which are the original uncorrected variables
 */
srtt[2];                 // Array for smoothed RTT (primary and alt)
mdev[2];                 // Array for mean deviation of RTT (primary and alt)
acc_mrtt;                // Latest measured accurate RTT (excl. delayed ACK)
error_;
sign_ = 0;

/* Initialize EWMAs */
srtt[1] = FALSE;        // holds the alt smoothed RTT, but if FALSE disables alt's.
mrtt[1] = 0;
// srtt[0] and mdev[0] will have been initialized elsewhere (resp. to acc_mrtt and 0)

{ /* Per ACK */
    error_ = acc_mrtt - srtt[0];
    // Check for inlier or outlier on opposite side to the alt srtt
    if (  (abs(error_) <= K2 * mdev[0])                                // Inlier
            ||  (srtt[1] && (sign_ * error_ <= K2 * mdev[0])) ) {      // Opp. outlier
        if (srtt[1]) {
            mdev[1] += G2 * (abs(acc_mrtt - srtt[1]) - mdev[1])); // Update alt mdev
            if (mdev[1] > mdev[0])) {    // suppress alt's if worse mean deviation
                srtt[1] = FALSE;
            } else {                     // Continue to maintain alt srtt
                srtt[1] += G1 * (acc_mrtt - srtt[1]);
            }
        }
    } else {                             // Outlier (on same side if alt srtt enabled)
        if (srtt[1]) {                   // Continue to maintain alt srtt
            mdev[1] += G2 * (abs(acc_mrtt - srtt[1]) - mdev[1]));
            srtt[1] += G1 * (acc_mrtt - srtt[1]);
        } else {                         // Initialize alt's
            mdev[1] = mdev[0] * K1;      // Inflate by K1 for hysteresis
            srtt[1] = acc_mrtt;
            sign_ = sgn(error_);         // Record which side the outlier is on
        }
    }
    
    // Regular update of non-alt srtt and mdev  (order-significant)
    mdev[0] += G2 * (abs(acc_mrtt - srtt[0]) - mdev[0]));
    srtt[0] += G1 * (acc_mrtt - srtt[0]);
}
\end{verbatim}
\paragraph{Pseudocode Walk-Through} 

The purpose of each variable is assumed to be self-explanatory from the comments in the pseudocode. So this explanation will focus on the logic, which all executes on a per-ACK basis.

The structure of the main `if' block consists of two inter-locked conditions in a bistable flip-flop arrangement:
\begin{itemize}
    \item Whether the latest RTT is an outlier with respect to \texttt{srtt[0]} (and, whether it outlies on the same side as the alternative EWMA, if one is enabled\footnote{This prevents an alternative EWMA being started from an outlier on one side of the original \texttt{srtt[0]} EWMA, but kept enabled by outliers on the other side. This would otherwise be a common occurrence given outliers at the top of a TCP sawtooth are typically followed by outliers at the bottom of the next sawtooth.});
    
    \item Whether alternative EWMAs are enabled (i.e.\ whether srtt[1] is non-zero).
\end{itemize}
Within each branch of the ``outlier?'' condition, there is an ``alt's enabled?'' condition, so the code flip-flops as follows:
\begin{itemize}
    \item If ``outlier?'' is true, and ``alt's enabled?'' is not, alt's are enabled.
    
    \item If ``outlier?'' is false, and ``alt's enabled?'' is true, then alt's are tested to see if they should be disabled (they are disabled if the alternative \texttt{mdev[1]} is worse (larger) than the original \texttt{mdev[0]}). 
\end{itemize}

In the stable state when alternative EWMAs are enabled, they are maintained as normal. And in the stable state when alternative EWMAs are disabled, they are not. This prevents the code doing unnecessary per-ACK work. In production code, a tolerance could easily be added to disable maintenance of alternative EWMAs sooner; when they are hardly any better than the originals.

Once the main `if' block completes, the original EWMAs are updated, as always.

The macros SRTT and MDEV are provided for other code to use. They compare the mean deviation of the two EWMAs and use the best (smallest).  In production code, to make these macros more efficient, when \texttt{srtt[1]} is set to FALSE to disable alternative EWMAs, \texttt{mdev[1]} could be set to a special `inifinite' value, so that these macros would not need to test \texttt{srtt[1]} as well as \texttt{mdev[1]}.

