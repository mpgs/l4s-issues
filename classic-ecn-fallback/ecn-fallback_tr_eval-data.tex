% !TeX root = ecn-fallback_tr.tex
% ================================================================
\section{Evaluation}\label{ecn-fallbacktr_evaluation}

A large number of evaluation results are available
online\footnote{\protect\label{note:eval_url}At \href{https://l4s.net/ecn-fbk/}{l4s.net/ecn-fbk/} or via the Evaluations section
of the L4S landing page at \href{https://riteproject.eu/dctth/\#eval}{riteproject.eu/dctth/\#eval}.},
with more being added continually. This section gives a brief summary of
the results so far. It solely evaluates the passive detection algorithm,
and none of the tests yet exercise detection of self-limiting or idling.

% ----------------------------------------------------------------
\subsection{Experimental Conditions} 
\begin{description}
	\item[Topology:] Dumbell. Experiments were conducted using 2 pairs of
	Linux hosts as sender and receiver; the sender in each pair supporting a
	different congestion control behaviour. The two senders were connected to
	separate interfaces of a Linux router, with one output interface configured 
	to have various AQMs applied. Then that interface was connected to a bridge, 
	with two ports in turn connected to the two Linux receivers.
		
	\item[Software versions:] All Linux machines (hosts and router) were
	running a modified Linux kernel v5.4-rc3
	[\href{https://github.com/L4STeam/linux/blob/testing/08-04-2020/Makefile}{testing/08-04-2020}], 
	which included v2.2 of the Classic ECN AQM Fall-back algorithm.
	Default configuration parameters were used for all software.
	
	\item[Metrics:] Unless otherwise stated for particular experiments, the
	following metrics were tracked for each Prague flow (per-flow
	variables were not tracked for `background' Cubic flows):
	
	\begin{itemize}
		\item \texttt{classic\_ecn} score (per RTT), which depends on the 5
		metrics below that are not in [brackets];
		\item Measured RTT, \texttt{acc\_mrtt} (per ACK);
		\item Adaptively smoothed RTT, \texttt{fbk\_srtt} (per ACK);
		\item {[For comparison, the pre-existing fixed gain smoothed RTT, \texttt{srtt} (per ACK)]};
		\item Adaptively smoothed mean RTT deviation, \texttt{fbk\_mdev} (per ACK);
		\item Min RTT, \texttt{rtt\_min} (per change);
		\item Slow start threshold, \texttt{ssthresh} (per change);
		\item {[Congestion window, \texttt{cwnd} (per change)]};
	\end{itemize}
	\item The following metrics were measured at the AQM for all traffic:
	\begin{itemize}
		\item The throughput per flow, which was sampled per packet each time
		taking a rolling window of the last 1\,s of transmitted data. Throughput
		was only measured individually for long-running flows; in experiments
		with short flows, the throughput of all short flows in a class was measured as if they
		were one aggregate flow;
		\bob{Change to rolling sum over 100\,ms for 20\,s}
		\item The average throughput per flow in each class of flows---scalable
		or Classic (1\,s rolling window, sampled per packet). Short flows were
		excluded from the average throughput per flow.
		\item Link utilization (1\,s rolling window, sampled per packet);
		\item Queue delay, measured per packet;
		\item Drop and marking probability, sampled every 16 base RTTs.
	\end{itemize}
	\item[Traffic scenarios:] Simple traffic scenarios with equal RTT
	long-running flows are used to measure steady-state rate for `fairness'
	metrics, irrespective of whether such scenarios are typical on the
	Internet. Scenarios with short flows and a mix of short and long flows
	are introduced later, including staggered or simultaneous start-up of
	long-running flows.
\end{description}

% ----------------------------------------------------------------
\subsection{`Fairness' with Long-Running Flows}\label{ecn-fallbacktr_eval_fairness}

The two charts in \autoref{fig:ecn-fallbacktr_eval_before_after} give a
high level picture of the improvement in flow rate balance ('fairness')
that the passive Classic ECN AQM detection and fall-back algorithm gives. 
They show TCP Prague competing with TCP Cubic-ECN over a shared queue CoDel
AQM using a \(5\times5\) matrix of different link rates and base RTTs.
Each plot (each tall thin cross) shows the \(1^{\mathrm{st}}\)
percentile, mean and \(99^{\mathrm{th}}\) percentile of the normalized
rate per flow (see definition below). A value of 1 (\(10^0\)) is the
ideal. The left and right-hand charts show the outcome with the algorithm
disabled and enabled respectively. In all cases, it can be seen that
enabling the algorithm causes TCP Prague to pretty closely balance its
flow rate with TCP Cubic-ECN.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.45\linewidth]{200410_1x1_whisker200s_baseline_v2}
  \includegraphics[width=0.45\linewidth]{200410_1x1_whisker200s_ecn_fallback_v2}
\caption{Comparison between Classic ECN AQM Fall-back algorithm when
disabled (left) and enabled (right) in TCP Prague over 25 combinations of
link-rates and base RTTs. In both cases, the long-running TCP Prague flow
competes with a long-running TCP Cubic ECN flow over a CoDel AQM.
Normalized rate per flow is defined in the
text.}\label{fig:ecn-fallbacktr_eval_before_after}
\end{figure}

Normalized rate per flow is defined as the ratio of the flow rate, \(x\)
relative to an `ideal' rate. The ideal rate is defined as the link
capacity divided by \(n\), where \(n\) is the number of flows (\(n=2\) in
all cases in \autoref{fig:ecn-fallbacktr_eval_before_after}). Thus,
normalized rate per flow \(= x/(C/n)\). In this `fairness' experiment,
rate measurements were taken after allowing flows to stabilize for
\(T\)\,s, with \(T=5+x*R/100,000\), where \(x\) is the flow rate in [b/s]
and \(R\) is the RTT in [s].

It can be seen that the left-hand chart is similar to
\autoref{fig:ecn-fallbacktr_rate_flow} in
\S\,\ref{ecn-fallbacktr_problem}, which we originally used to illustrate
the coexistence problem. The link-rates and base RTTs are the same, but
in \autoref{fig:ecn-fallbacktr_rate_flow} PI2 was used as the Classic AQM
and DCTCP was used as the L4S congestion control. In
\autoref{fig:ecn-fallbacktr_eval_before_after} CoDel (not FQ\_CoDel) is
used as the Classic AQM, because it is likely to be the most difficult
single-queue Classic AQM for the algorithm to distinguish from an L4S AQM
(see \S\,\ref{ecn-fallbacktr_detection_params} for reasoning).

\begin{figure}
  \centering
  \includegraphics[width=0.49\linewidth]{codel_12m_50ms_1v1_classic_ecn}
  \includegraphics[width=0.49\linewidth]{codel_12m_50ms_1v1_throughput}
\caption{A typical example of the evolution of the \texttt{classic\_ecn}
score (left) in a long-running TCP Prague flow while competing with 1
long-running Cubic flow in a CoDel AQM bottleneck. The algorithm detects a Classic AQM
and switches to classic ABE-Reno behaviour
(\(\mathtt{classic\_ecn}\ge1\)) after about 1\,s. The resulting
throughput of each flow is shown on the right [12\,Mb/s link; 50\,ms base
RTT].}\label{fig:ecn-fallbacktr_codel_12m_50ms_1v1_classic_ecn}
\end{figure}

In every case, the algorithm rapidly detects a Classic ECN AQM and
switches TCP Prague over to its ABE-Reno response to ECN marks, which
then competes roughly equally with Cubic.
\autoref{fig:ecn-fallbacktr_codel_12m_50ms_1v1_classic_ecn} is a typical
example time series at 12\,Mb/s and 50\,ms base RTT. In the left-hand plot, after 0.5\,s
the \texttt{classic\_ecn} variable can be seen starting to move off its
\texttt{-L\_STICKY} floor (-8) towards classic. It has already fully
transitioning to classic (at \(\mathtt{classic\_ecn} = 1\)) after 1\,s  (about
50 base round trips), then continues onward to \(\mathtt{classic\_ecn} = 9\) 
by 1.5\,s after the flow started.

The flow throughputs are shown on the right-hand side of
\autoref{fig:ecn-fallbacktr_codel_12m_50ms_1v1_classic_ecn}. The Prague
flow (in blue) fills the available capacity after about 1.5\,s; at about
the same time as its \texttt{classic\_ecn} score hits its maximum
`stickiness' of 9. Therefore, for the rest of the time, it behaves as
ABE-Reno, and competes on roughly equal terms with the Cubic-ECN flow (in
black).

% ----------------------------------------------------------------
\subsection{Fairness with Staggered Long-Running
Flows}\label{ecn-fallbacktr_eval_fairness_staggered}

It is necessary to test for cases in which the fall-back algorithm might
never see the true minimum RTT. This can occur if one flow starts while a
long-running flow is already maintaining a standing queue.

\begin{figure}
  \centering
  a) \includegraphics[width=0.43\linewidth]{codel_40m_20ms_stag_1v9_classic_ecn}
  c) \includegraphics[width=0.43\linewidth]{codel_40m_20ms_stag_1v9_depth}\\
  b) \includegraphics[width=0.43\linewidth]{codel_40m_20ms_stag_1v9_throughput}
  d) \includegraphics[width=0.43\linewidth]{codel_40m_20ms_stag_1v9_mdev}
\caption{At 10\,s a long-running Prague flow joins a standing queue
built by 9 Cubic flows over a CoDel AQM. Four variables are tracked: a)
\texttt{classic\_ecn} score; b) per-flow throughput; c) queue delay
(actual (yellow/green) and sender's estimate (blue)); d) Mean RTT
deviation. [40\,Mb/s link; 20\,ms base
RTT]}\label{fig:ecn-fallbacktr_codel_40m_20ms_stag_1v9}
\end{figure}

In the previous section, both flows started simultaneously, whereas
\autoref{fig:ecn-fallbacktr_codel_40m_20ms_stag_1v9} shows a case where a
single Prague flow joins 9 Cubic-ECN flows after 10\,s. It can be seen
from \autoref{fig:ecn-fallbacktr_codel_40m_20ms_stag_1v9}a) that the
Prague flow switches to Classic ABE-Reno behaviour about 0.75\,s after it
starts. And \autoref{fig:ecn-fallbacktr_codel_40m_20ms_stag_1v9}b) shows
that the throughput of the Prague flow (blue) competes reasonably `fairly'
with the Cubic flows (rate ratio of roughly 1.75).

Thus the fall-back algorithm works despite not correctly measuring the
queue delay, as we shall now explain with the aid of
\autoref{fig:ecn-fallbacktr_codel_40m_20ms_stag_1v9}c), which overlays
the queue delay as measured at the AQM (yellow/green)\footnote{Yellow and
green separate out the queue as measured on arrival of ECT(0) and ECT(1)
packets respectively. Being a single queue, they both give the same
reading in this case (although they are time-shifted due to slippage
between the system clocks used for the two measurements).} with the
smoothed queue delay estimated by the Prague sender's fall-back algorithm
(blue), which uses only end-to-end measurements. It can be seen that Prague's
e2e measurements persistently under-estimate the queue delay by the same
amount, which represents its over-estimate of the min RTT, due to only
ever having seen a standing queue. Nonetheless, this does not cause the
algorithm to switch back to scalable L4S behaviour, for three reasons: 
\begin{enumerate}[nosep]
	\item The queue delay estimate is not sufficiently incorrect to drop below the
	queue delay threshold (\(\mathtt{D0}=2\,\)ms) shown as a a red dashed horizontal);
	
	\item Even if the estimate did drop below the threshold, the 
	algorithm only takes note of queue delay above the threshold, not below 
	(specifically because the min RTT is unreliable);
	
	\item The algorithm also uses RTT variability, and the sender's
	instantaneous measurements still lead to a value of mean deviation that
	is higher than the \(750\,\mu\)s threshold, as illustrated by the red
	dashed horizontal in
	\autoref{fig:ecn-fallbacktr_codel_40m_20ms_stag_1v9}d).
\end{enumerate}

% ----------------------------------------------------------------
\subsection{Switching AQM Mid-Flow}\label{ecn-fallbacktr_eval_aqm_switch}

Experiments were also conducted to determine the effect of a change in
AQM mid-flow. Though unlikely, an AQM change can happen, e.g.\ if the
path of a flow re-routes, or if a different buffer becomes the most
bottlenecked on the path. Nonetheless, the purpose of these experiments
was more to double-check that the fall-back algorithm does not exhibit any
unexpected behaviour during such an event. Nonetheless, it was also
interesting to see just how quickly the algorithm could detect such a
change.

Figures \ref{fig:ecn-fallbacktr_codel_dualpi2_40m_10ms_1Lv9} \&
\ref{fig:ecn-fallbacktr_dualpi2_codel_40m_10ms_1Lv9} show the outcome in
one example scenario (40\,Mb/s link rate, 10\,ms base RTT). The scenarios
shown differ only in which AQM was applied first.

\begin{figure}
  \centering
  a) \includegraphics[width=0.43\linewidth]{codel_dualpi2_40m_10ms_1Lv9_classic_ecn}
  c) \includegraphics[width=0.43\linewidth]{codel_dualpi2_40m_10ms_1Lv9_depth}\\
  b) \includegraphics[width=0.43\linewidth]{codel_dualpi2_40m_10ms_1Lv9_throughput}
  d) \includegraphics[width=0.43\linewidth]{codel_dualpi2_40m_10ms_1Lv9_mdev_pace}
\caption{At 10\,s the AQM switches from CoDel to DualPI2, while 1 Prague \& 9 Cubic-ECN long-running flows proceed. Also a low background load of web-like (type L) Prague flows arrive continually. Four variables are tracked: a)
\texttt{classic\_ecn} score; b) per-flow throughput; c) queue delay
(actual (yellow/green) and sender's estimate (blue)); d) Mean RTT
deviation. [40\,Mb/s link; 10\,ms base
RTT]}\label{fig:ecn-fallbacktr_codel_dualpi2_40m_10ms_1Lv9}
\end{figure}

\begin{figure}
  \centering
  a) \includegraphics[width=0.43\linewidth]{dualpi2_codel_40m_10ms_1Lv9_classic_ecn}
  c) \includegraphics[width=0.43\linewidth]{dualpi2_codel_40m_10ms_1Lv9_depth}\\
  b) \includegraphics[width=0.43\linewidth]{dualpi2_codel_40m_10ms_1Lv9_throughput}
  d) \includegraphics[width=0.43\linewidth]{dualpi2_codel_40m_10ms_1Lv9_mdev_pace}
\caption{As \autoref{fig:ecn-fallbacktr_codel_dualpi2_40m_10ms_1Lv9}, except the order of the AQM is reversed}\label{fig:ecn-fallbacktr_dualpi2_codel_40m_10ms_1Lv9}
\end{figure}

The backgrounds of the plots of the \texttt{classic\_ecn} scores (Figures
\ref{fig:ecn-fallbacktr_codel_dualpi2_40m_10ms_1Lv9}a) \&
\ref{fig:ecn-fallbacktr_dualpi2_codel_40m_10ms_1Lv9}a) have been coloured
to show the regions where the score should transition to in green (if the
algorithm correctly detects the type of AQM), and where it should
transition away from in amber or red. Red represents the unsafe situation
where a Prague flow incorrectly detects that the AQM is L4S when it is Classic.
Amber represents the safe but incorrect situation where a flow
incorrectly detects a Classic AQM when it is L4S. It can be sesen that in
both cases, the Prague flow rapidly detects and switches to the correct
AQM soon after startup, and again, soon after the switch over.

The evolution of the \texttt{classic\_ecn} score in a few of the short
Prague flows can be seen starting at their minimum value and rarely
moving before the flow finishes (by design). In
\autoref{fig:ecn-fallbacktr_dualpi2_codel_40m_10ms_1Lv9}a) a couple of
slightly longer short flows can be seen starting to move towards the
classic end of the scoring range, but only one (a blue one starting
around19\,s) lasts long enough to get there.

In both Figures \ref{fig:ecn-fallbacktr_codel_dualpi2_40m_10ms_1Lv9}b) \&
\ref{fig:ecn-fallbacktr_dualpi2_codel_40m_10ms_1Lv9}b) it can be seen
that the Prague flow's throughput seems unperturbed by the switch of AQM.
This is because it rapidly detects the type of AQM and its fall-back
algorithm quickly adapts between scalable and classic behaviour (or
\emph{vice versa}) as appropriate for the detected AQM.

Incidentally, the yellow plot tracks the total throughput of all short
flows (all Prague in this scenario). There is no expectation that their
aggregate throughput will relate in any way to the rates of the
long-running flows, because short flows act as a semi-unresponsive
aggregate.

Figures \ref{fig:ecn-fallbacktr_codel_dualpi2_40m_10ms_1Lv9}c) \&
\ref{fig:ecn-fallbacktr_dualpi2_codel_40m_10ms_1Lv9}c) give a vivid
illustration of the difference in the queuing delay that each AQM can
achieve. Over the Codel \(\rightarrow\) DualPI2 switch, the classic queue
induced by the Cubic-ECN traffic stays roughly the same, growing only
slightly, as PI2 takes over from CoDel. In contrast, once the Prague flow
detects it is in the L4S queue within the coupled DualPI2 AQM structure,
its queue rapidly reduces. The chart shows the 2\,ms threshold chosen for
the algorithm to distinguish between Classic and L4S queues, and it can
clearly be seen how the queueing delay of ECT(1) packets (green) and the
Prague flow's estimate of the queueing delay (blue) sit above or below
this threshold depending on whether the AQM is respectively Classic or
L4S.

Similarly, Figures \ref{fig:ecn-fallbacktr_codel_dualpi2_40m_10ms_1Lv9}d)
\& \ref{fig:ecn-fallbacktr_dualpi2_codel_40m_10ms_1Lv9}d) plot the
algorithm's estimate of the mean deviation of the queue, again clearly
showing that the variability of the Classic and L4S AQMs sit respectively
above and below the \(750\,\mu\)s threshold chosen to tell them apart.
Incidentally, the small coloured `scribbles' rising from the x-axis are
the estimates of mean RTT deviation starting to form in each short flow,
up to the point that each one completes.

In summary, the fall-back algorithm clearly detects a changed environment
rapidly, and switches fast so that the size of the congestion control's sawteeth 
conform to the 'rules' it expects other flows to be
following in each environment. Consequently, there is no discernible
change in per-flow throughput across the switch-over, despite the Classic
AQM not having any logic for coexistence between Classic and and scalable
flows.

% ----------------------------------------------------------------
\subsection{Classic ECN AQM Detection under a Range of Traffic Mixes}\label{ecn-fallbacktr_eval_detection_mix}

The online evaluations already
mentioned\textsuperscript{\ref{note:eval_url}} were used to test the
fall-back algorithm in a range of traffic scenarios. Each traffic scenario tests
one of the following flow patterns for Prague against another of this same 
list of flow patterns for Cubic-ECN:
\begin{description}
	\item[0:] denotes zero flows;
	\item[1:] denotes 1 long-running flow;
	\item[9:] denotes 9 long-running flow;
	\item[L:] denotes a `low load' of synthetic web-like short flows following an
	exponential arrival process. `Low load' equates to 1 request per second
	for the 4\,Mb/s link rate, scaled up proportionately for high rate links
	(e.g.\ 50 req/s for the 200\,Mb/s link). A lower load is used as the
	worst case, because the congestion controls and AQMs cannot settle at any
	one operating point before the load changes again. Every request opens a
	new TCP connection, closed by the server after sending data with a size
	according to a Pareto distribution with \(\alpha= 0.9\) and minimum and
	maximum sizes of 1\,KB and 1\,MB.
	\item[1L:] denotes 1 long-running flow combined with load 'L'.
\end{description}

These traffic descriptors are placed either side of a colon to denote
each traffic scenario, with the Prague flows always given first. For
example, 1:0 denotes 1 long-running Prague flow alone; while 9:L means 9
long-running Prague flows plus a low load of short, web-like Cubic-ECN
flows. 

This results in a \(4\times5\) matrix of traffic scenarios because, although there 
are 5 descriptors because there is little point in testing zero Prague flows. Each
traffic scenario was then tested in each of the combinations of 5 link rates
and 5 base RTTs already seen in \autoref{fig:ecn-fallbacktr_eval_before_after}
Thus each AQM scenario
was tested over a matrix of \((4\times5)\times(5\times5)=500\) different
traffic and environment scenarios. These are clearly far too numerous to include
exhaustively in this paper, but the general trend of the results will be described. 

Online, there is a `traffic light' visualization of all these
experiments.\textsuperscript{\ref{note:eval_url}} Each experiment was run
for 20\,s and given a colour in a large matrix of coloured squares
depending on the success of Classic ECN AQM detection within that time:
 \begin{description}
	\item[Green] denotes a true positive or true negative, (i.e.\ correctly detecting the AQM, whether Classic or L4S;
	\item[Amber] denotes a false positive, i.e.\ detecting a Classic AQM when the AQM is actually L4S;
	\item[Red] denotes a false negative, i.e.\ detecting an L4S AQM when the AQM is actually Classic;
\end{description}
False negatives are coloured red because it could be unsafe for TCP
Prague to behave as a scalable flow in a Classic AQM, if it is in a
shared queue. In such a case, TCP Prague would then outcompete any
Classic flows sharing the same bottleneck. In contrast, with false
positives, TCP Prague responds to ECN marking with a large classic
reduction, potentially under-utilizing the link, but not harming other
flows.

\begin{quote}
Note that, failure of the detection algorithm only leads to the potential
for harm not necessarily actual harm. For instance, detection is deemed
to have failed if it does not reach the correct state within the 20\,s
duration of each experiment. And in many experiments the algorithm has to
respond to a change after 10\,s, leaving it only a further 10\,s to
succeed. The choice of 20\,s and 10\,s as deadlines 
was a subjective trade-off between how long an effect is likely to be noticeable
and the impact on the run-time of all the experiments.

Often, given a little longer, the algorithm correctly detects the AQM.
Also, detection is deemed to have failed if at least a `significant
minority' of short flows switch to Classic behaviour by the time they
complete, even though they are over an L4S AQM. All these transient
effects are rarely detectable on plots of the resulting flow throughput,
let alone detectable to an end-user. 
\end{quote}

A high level summary of the results so far follows:
\begin{itemize}
	\item The fall-back algorithm correctly detected a CoDel AQM as Classic in
	497 of the 500 scenarios. All three false negatives (0.6\% of scenarios)
	involved zero Cubic-ECN flows, so there were no cases where any Cubic-ECN
	flow was at risk of being outcompeted by TCP Prague.
	
	\item The fall-back algorithm correctly detected that a DualPI2 AQM was
	L4S in 361 of the 500 scenarios. There were no unsafe false negatives,
	but the balance of the scenarios (139 or 28\%) resulted in a false
	positive. Of these:
	\begin{itemize}
		\item 75 (15\%) occurred at the 4\,Mb/s link rate, because the
		serialization delay of one packet at that rate is 3\,ms, which is greater
		than the algorithm's queue depth threshold.%
		\footnote{\label{note:eval_heuristic_plan}It is planned to address this 
		in a future revision of the algorithm by adding a heuristic that increases the 
		threshold slightly for these low flow rates.}
		
		\item 52 (10\%) occurred at the 12\,Mb/s link rate, for a similar reason,
		i.e.\ the threshold queue depth of 2\,ms is only two packet serialization
		delays at this link rate. Detection was correct in a large majority of
		the single flow tests, but only in a minority of those with multiple
		simultaneous flows (whether long-running or a mix of long and short).%
		\textsuperscript{\ref{note:eval_heuristic_plan}}
		
		\item 11 (2\%) occurred at the 40Mb/s link rate; more than half (6)
		occurring with the highest base RTT (100\,ms).\footnote{\label{note:eval_smoothing_plan}%
		Initial experiments have
		been undertaken smoothing the mean deviation of the RTT twice as fast
		(\(\mathtt{g\_mdev} = \mathtt{g\_srtt}\) rather than \( = \mathtt{g\_srtt} * 2\)). This seems to solve this
		problem without making detection of Classic worse, but a full run of
		experiments is needed to verify this.}

		\item 1 false positive occurred in the 50 scenarios at link rates above 40\,Mb/s.
	\end{itemize}
	\item Results when the AQM was switched from CoDel to DualPI2 half way
	through the run (after 10\,s) produced similar results to those over
	DualPI2 alone. However there were more false positives at the largest base
	RTT of 100\,ms, because only introducing the DualPI2 AQM after 10\,s left
	only half as long to detect it and switch to it before the 20\,s deadline. 
	Also 2 false negatives crept in. No long-running Cubic
	flows were present in either of these false-negative cases, so again
	there was no safety issue.
	
	\item When the AQM was switched from DualPI2 to CoDel half way through
	the run (after 10\,s), again the outcome was very similar to that with DualPI2
	alone, with false positives in similar types of 
	scenarios. However, this time, more false negatives appeared as well.
	Again, no long-running Cubic flows were present in any of these
	false-negative cases with one exception (1:1 at the maximum link rate and
	base RTT tested\footnote{Once the two planned alterations mentioned earlier are tested, it
	is expected that many or hopefully all of these false results will
	disappear.}).
\end{itemize}

Failures to detect the correct AQM were typically transient and, as noted
earlier, they only indicate the possibility of harm, not actual harm. No
discernible effect could be noticed on throughput balance in any of the
cases with a false result, whether false positive (amber) or false
negative (red). This is evidenced by the throughput results in
\autoref{fig:ecn-fallbacktr_eval_before_after}.

In particular, the failures at low link rate typically involved
congestion windows that were already close to TCP's minimum of 2
segments. Therefore it made little difference whether an attempt to
reduce \texttt{cwnd} was large or small, because in practice only small
reductions were possible.


