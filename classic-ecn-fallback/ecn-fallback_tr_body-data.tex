% !TeX root = ecn-fallback_tr.tex
% ================================================================
\section{The Coexistence Problem}\label{ecn-fallbacktr_problem}

As the name implies, the Low Latency Low Loss Scalable throughput (L4S) architecture~\cite{Briscoe16a:l4s-arch_ID} is intended to enable incremental deployment of scalable congestion controls, which in turn are intended to provide very low latency and loss. 

Since 1988, when TCP congestion control was first developed, it has been known that it would hit a scaling problem. Footnote 6 of Jacobson \& Karels \cite{Jacobson88b:Cong_avoid_ctrl} said ``We are concerned that the congestion control noise sensitivity is quadratic in \(w\) but it will take at least another generation of network evolution to reach window sizes where this will be significant.'' The footnote went on to say, ``If experience shows this sensitivity to be a liability, a trivial modification to the algorithm makes it linear in \(w\).''

By the end of the 1990s that scaling problem had become very apparent~\cite{IETF_RFC3649:HSTCP}. A ``trivial modification to the algorithm'' would indeed make it linear, which is the definition of a scalable congestion control. However, the problem was not how to modify the algorithm, it was how to deploy it. Such a linear congestion control would not coexist with all the traffic on the Internet that had evolved in coexistence with the original TCP algorithm.

A scalable congestion control induces frequent congestion signals, and the frequency remains invariant as flow rate scales over the years. Therefore, modern scalable congestion controls, such as DCTCP~\cite{Alizadeh10:DCTCP_brief}, use Explicit Congestion Notification (ECN) rather than loss to signal congestion. They use the same ECN codepoints as the original ECN standard~\cite{IETF_RFC3168:ECN_IP_TCP}, but they induce much more frequent ECN marks~\cite{Briscoe15f:ecn-l4s-id_ID} than Classic (Reno-friendly) congestion controls.

Thus, if a scalable congestion control finds itself sharing a queue with a congestion control that conforms to the `Classic' definition of ECN as equivalent to loss~\cite{IETF_RFC3168:ECN_IP_TCP}, the Classic flow will imagine that there is heavy congestion and back off its flow rate. It will not actually starve itself, but
it will reduce to a rate that can be 4--16 times less on average than any competing L4S flow
(\autoref{fig:ecn-fallbacktr_rate_flow}).

\begin{figure}[h]
  \centering
  \includegraphics[width=0.7\linewidth]{rate_flow}
  \caption{Quantification of the Rate Imbalance Problem between L4S and Classic ECN flows in a Classic ECN Queue. Flow rates are of long-running flows after a period of stabilization and normalized relative to precisely equal flow rates, 1 DCTCP vs. 1 ECN-Cubic, equal base RTTs, single queue PI\(^2\) AQM; default parameters (see \S\,\ref{ecn-fallbacktr_eval_fairness} for full experiment details.)}\label{fig:ecn-fallbacktr_rate_flow}
\end{figure}

Coexistence is only a problem if the bottleneck is a shared queue that supports Classic ECN marking. It is not known whether any shared queue Classic ECN AQMs are operational on the Internet, and so far no evidence has been forthcoming from the search for one. However, for L4S to proceed through standardization, it has to be assumed that such AQMs do exist or that they might exist. Therefore scalable congestion controls ought to respond appropriately if they detect a classic ECN AQM. In 2015, this was codified as one of the `Prague L4S' requirements for scalable congestion controls, which have since been adopted by the IETF~\cite{Briscoe15f:ecn-l4s-id_ID}. That requirement sets the problem that motivates the present report.

Other aspects of the L4S architecture already address coexistence in all the other possible cases, viz:
\begin{itemize}
	\item If the bottleneck does not support any form of ECN (as is the case
	for the vast majority of buffers on the Internet), the only sign of
	congestion will be loss. It is easy to ensure that scalable congestion
	controls respond to loss in a Reno-friendly way, and all known scalable
	congestion controls do so (this is top of the list of ``Prague L4S
	requirements''~\cite{Briscoe15f:ecn-l4s-id_ID}).
	
	\item If the bottleneck applies per-microflow\footnote{A microflow is a data-flow 
	between two application end-points that is identified by the 5-tuple of source and 
	destination addresses \& ports plus the protocol. Per-flow queuing can be 
	configured for other definitions of `flow', but per-microflow is the most common.} 
	scheduling, it enforces
	coexistence without the need for the present algorithm. There are two
	sub-cases:
	\begin{itemize}
		\item If there is a Classic AQM in each per-flow queue (understood to be
		a common deployment with FQ\_CoDel~\cite{Hoeiland18:fq-codel_RFC} and
		COBALT~\cite{Palmei19:Cobalt}, which is used in CAKE), then the detection
		algorithm in the present paper ought to detect it and switch to Classic
		behaviour, which could provide performance benefits;
		
		\item If the AQM in each per-flow queue supports L4S by detecting the L4S
		ECN identifier, then the full benefits of L4S will be available without
		the present algorithm, which will remain quiescent;
	\end{itemize}
	
	\item If the bottleneck supports the DualQ Coupled
	AQM~\cite{Briscoe15e:DualQ-Coupled-AQM_ID}, that will ensure that L4S and
	classic flows coexist and the full benefits of L4S will be available
	without the present algorithm, which will again remain quiescent.
\end{itemize}

% ================================================================
\section{Modular Approach}\label{ecn-fallbacktr_modular}

% ----------------------------------------------------------------
\subsection{Modularity Requirements}\label{ecn-fallbacktr_modular_reqs}

An operator might want to determine the type of AQM on the path
\textbf{in-band}, that is within live traffic, or \textbf{out-of-band} using
test traffic.

An aim of in-band testing is to detect the type of AQM fast
enough%
\footnote{Perhaps within a dozen or so round trips, given the original
	paper on TCP Cubic\cite{cubic} described convergence as
	`short' when it took one or two hundred rounds (assuming flows even last that
	long).} %
that a flow can start with the L4S behaviour but if necessary change
over to Classic behaviour in the early stages of convergence.

However, a server operator might not want to trigger any change in behaviour.
For instance, a CDN operator might want to use in-band or out-of-band testing
purely to check the likelihood that the problem even exists over the paths they
serve.

Therefore, a complete algorithm needs to work in two separable stages:
\textbf{detection} then if necessary \textbf{fall-back} to Classic behaviour.
More generally, fall-back ought to be called \textbf{changeover} because, as
detection continues, the algorithm might need to reverse the change (either
because the first change was premature, or because the bottleneck has changed).

Detection might purely measure the traffic's characteristics (\textbf{passive}),
or it might alter the way traffic is sent (\textbf{active}), e.g.\ altering
send-timing, the sizes of certain packets, their markings, or adding extra
probes. Active techniques tend to provide more certainty at the expense of
altering live traffic. So passive detection is preferable at first, but if
experimentation finds it is insufficient, active techniques could be kept in
reserve as a double-check just before a changeover actually occurs, perhaps only
in more challenging scenarios, e.g.\ high RTT, low link rate or when a bursty
radio link appears to be present on the path.

Out-of-band testing is not applied to live traffic, so it can typically resort
to active techniques straight away (unless it could potentially harm other live
traffic).

The main body of this report is divided into the same modular sections; on
passive detection (\S\,\ref{ecn-fallbacktr_detection}), active detection (both
out-of-band in \S\,\ref{ecn-fallbacktr_OOB_active} and in-band in
\S\,\ref{ecn-fallbacktr_active}) and change-over
(\S,\ref{ecn-fallbacktr_changeover}). Different modules can then be mixed and
matched to produce different solutions, one of which (so far) is evaluated in
\S\,\ref{ecn-fallbacktr_evaluation}.

% ----------------------------------------------------------------
\subsection{Modular Code Structure}\label{ecn-fallbacktr_structure}

Beyond solving the coexistence problem, the following principles are proposed to structure the code for multiple purposes:
\begin{enumerate}
	\item Rather than fall-back being a binary switch between modes, it should be
	a gradual changeover, the more certain it is that the AQM supports classic ECN;
	\item Nonetheless, at either end of the spectrum of (un)certainty, there
	should be ranges where the CC behaves on the one hand purely scalably and on
	the other purely classically;
	\item Minimal additional persistent TCP state;
	\item The code should be structured with detection separate from changeover of
	behaviour, so that detection can eventually apply to more than one CC, while
	changeover is likely to be CC-specific;
	\item However, until the concept is proven, it will be OK initially to
	implement the whole algorithm within the TCP Prague CC module, and only
	rationalize it once mature.
\end{enumerate}

To simplify pseudocode, a float called \texttt{c} controls how much the CC
should behave as classic, from 0 (scalable) to 1 (classic). In practice this
might be an integer variable in the range 0 to
\texttt{CLASSIC\_ECN}. This will be driven from the variable \texttt{classic\_ecn}, which is defined as the outputof the detection algorithm.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.35\linewidth]{c-trans}
  \caption{Score-based transition, rather than modal switch, between Scalable and Classic behaviour.}\label{fig:ecn-fallbacktr_c-trans}
\end{figure}

The \texttt{classic\_ecn} indicator can continue beyond either end of this
range. as depicted in \autoref{fig:ecn-fallbacktr_c-trans} and tabulated below, in order to implement a degree of stickiness where
the CC algorithm behaves purely as L4S or purely classically.

\begin{tabular}{rcccll}
\hline
%                              &              & \(c\) &  \(=\)  &  -L\_STICKY        & Minimum meaningful value of \(c\)\\
\texttt{-L\_STICKY}         & \(\le\) & \texttt{classic\_ecn} &\(\le\) & \texttt{0}                            & Pure L4S behaviour\\
\texttt{0}                            & \(\le\) & \texttt{classic\_ecn} & \(\le\) & \texttt{CLASSIC\_ECN} & Transition between L4S and classic\\
\texttt{CLASSIC\_ECN}  & \(\le\) & \texttt{classic\_ecn} & \(\le\) & \texttt{C\_STICKY}         & Pure classic behaviour\\
%                              &              & \(c\) &  \(=\)  &  C\_STICKY         & Maximum meaningful value of \(c\)\\
\hline
\end{tabular}

It is up to the detection algo, not the CC algo, to maintain the
\texttt{classic\_ecn} variable. However, any particular CC algorithm can override
the default parameters of the detection algorithm. For instance, a CC module
could alter how 'sticky' the hysteresis is at either end by overriding the
default \texttt{L\_STICKY} and/or \texttt{C\_STICKY} parameters.
 
