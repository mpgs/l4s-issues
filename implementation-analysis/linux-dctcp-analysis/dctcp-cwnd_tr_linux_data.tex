% !TeX root = scalable-cc-analysis_tr.tex
% ================================================================
%\section{Theoretical DCTCP Equilibrium Window; Step Marking}

%\section{Theoretical DCTCP Equilibrium Window; Ramp Marking}

\section{Introduction}

The main novelty of DCTCP is to respond to the extent of congestion marking, unlike traditional congestion controls that respond to the existence of congestion. However, the way that DCTCP congestion control is implemented in Linux (and possibly in other operating systems) is a hybrid of the two approaches. It  \emph{measures} the extent of congestion, but it \emph{acts} in response to the existence of congestion.

This hybrid of approaches has raised concerns that it will lead to on-off behaviour, which might cause more burstiness than necessary. On the other hand, it might have subtleties that are beneficial. This paper models the Linux DCTCP implementation analytically to better understand it. This also gives the congestion response function of Linux DCTCP in more precise form than the idealized analyses in~\cite{Alizadeh10:DCTCP, Alizadeh11:DCTCP_Analysis, DeSchepper16a:PI2}, which better characterizes its steady-state rate and can be used in future to better understand its dynamics.

At the time of writing, this analysis applies equally to TCP Prague.

\section{Whoah, Stop Reading Now! The Analysis in this Paper is Flawed}

After writing this little paper, I realized the analysis is flawed. It defines \(X\) as the number of unmarked packets in the gap between the end of CWR state and the next mark (see schematic in next section). It calculates \(E(X)\) as \(1/p - 1\), but this is also the expectation of the number of unmarked packets between two marks. These two statements cannot both be true.

The problem is that the start of this gap is not arbitrary; it is a defined distance from the mark at the start of the previous window, and this distance in turn depends on the steady-state marking probability. 

To rework the analysis, the size of the gap needs to be smaller than the remainder after each discrete number of marks that could possibly have occurred within CWR state. Then the expectation of the size of the gap needs to be calculated over all those cases.

It may also be necessary to take into account the correlation between the marking probability and whether the scalable congestion control is in CWR state or not, as illustrated in the following schematic:

{\centering
  \includegraphics[width=\linewidth]{dctcp-linux-cwnd2}
}%

A brief literature survey was conducted to see if anyone has already prepared a good model. The ''Analytical Model of TCP Relentless Congestion Control''~\cite{Diana11:Relentless_Model} looks promising. However, it is flawed in a different respect; it uses the modelling approach of Padhye \emph{et al}~\cite{Padhye98:TCP_thruput_model}, which takes each period of additive increase as a whole multiple of the RTT on the presumption that it persists for many round trips.

\section{Linux DCTCP Equilibrium Window}

In response to the \emph{existence} of a single ECN mark, Linux DCTCP initiates congestion window reduction and switches into a Congestion Window Reduced (CWR) state. During the subsequent round trip it remains in that state, which suppresses any window increase or decrease whatever the extent of any further marking. The congestion control can only apply additive increase once the round of CWR state has ended, and then only for as long as no congestion marks arrive, because any subsequent mark will trigger the start of a new round of CWR state. This closely follows the way that the Explicit Congestion Notification (ECN) support~\cite{IETF_RFC3168:ECN_IP_TCP} was originally implemented in Linux, which in turn followed the way that Linux handled loss.

Whether in CWR state or not, Linux DCTCP continues to characterize the \emph{extent} of marking, which it uses to determine the size of its window reduction every time it enters CWR state. It measure the fraction of ECN marking, and once per round trip it updates an Exponentially Weighted Moving Average (EWMA) of these fractions.

%\begin{figure}
{\centering
  \includegraphics[width=\linewidth]{dctcp-linux-cwnd}
}%
%\caption{Schematic of Linux DCTCP moving in and out of Congestion Window Reduced (CWR) state (within and between grey boxes) in response to grey Congestion Experienced (CE) markings on a sequence of packets (the circles)}\label{fig:dctcp-linux-cwnd}
%\end{figure}
The schematic above illustrates the way Linux DCTCP switches in and out of CWR state in response to grey Congestion Experienced (CE) markings on a sequence of packets (the circles). As soon as a congestion marking is detected, the CWR state lasts for a window, \(W\), of packets, shown within a grey rectangle. Once DCTCP exits this state, it applies additive increase (AI) on each subsequent ACK without congestion feedback. But at the first congestion feedback, AI stops and CWR state is entered again. 

We represent the number of unmarked packets outside the CWR state by the random variable \(X\). Then, in a run of \(W+X\) packets, additive increase will be applied on \(X\) packets, and the additive increase on each of these \(X\) packets will be \(1/W\).\footnote{Actually, Linux measures its congestion window, \(W\), in units of whole segments, so a carry variable is used to accumulate all these small per packet increases until they amount to a whole segment.} So in steady state, the average increase per packet will be \(X/(W+X) * 1/W\).

The decrease that DCTCP applies per occurrence of CWR state is \(\alpha{W}/2\), where \(\alpha\) is the steady-state value of the EWMA, which tends to the equilibrium probability of the AQM marking any individual packet, which we shall denote by \(p\) (\(0\le p\le1\)). So in steady state the EWMA \(\alpha \rightarrow p\). Therefore the average decrease per packet will tend to \(1/(W+X) * pW/2\).

For balance between increase and decrease in the steady state, the per-packet increase and decrease will be equal, which allows us to derive the steady state window, \(W\) as a function of \(p\):
\begin{gather}
	X/W = pW/2\notag\\
	W = \sqrt{\frac{2X}{p}}.\label{eqn:cwnd_pX}
\end{gather}

The probability that there are \(X\) packets between one episode of CWR state and the next is the probability of a run of \(X\) unmarked packets followed by a marked packet, which is:
\begin{align*}
%	P(X = x) &= q^xp,
	P(X = x) &= (1-p)^xp.
\end{align*}
%where \(q = 1-p\).

The expectation of the length of such a run is
\begin{align*}
	  E(X) %&= \sum_{x=0}^\infty xP(X=x)\\
%	           &= (1-q)(q + 2q^2 + 3q^3 + \ldots)\\
%	           &= q + 2q^2 + 3q^3 + \ldots\\
%	           &\hphantom{= q\ }  -  \hphantom{2}q^2   - 2q^3 - \ldots;                &q<1\\
%	           &=  q + q^2 + q^3 + \ldots\\
%	           &= \frac{(1-q)}{(1-q)}\genfrac{}{}{0pt}{}{(q + q^2 + q^3 + \ldots)}{\phantom{(1)}}\\
%	           &= \frac{q}{(1-q)}\\
	           &= \frac{(1-p)}{p}
\end{align*}

Substituting into the earlier balance equation (\ref{eqn:cwnd_pX}), the steady-state window,
\begin{align}
	W %&= \sqrt{\frac{2X}{p}}\notag\\
	     &= \frac{\sqrt{2(1-p)}}{p}\label{eqn:cwnd_p}
\end{align}

It may help to compare this to the approximated response of an idealized DCTCP to ramp AQM marking in~\cite{DeSchepper16a:PI2}, which takes the form:
\begin{align}
	W &= \frac{2}{\pi},
\end{align}
with \(p\) replaced by \(\pi\), which can be thought of as the effective marking probability that Linux DCTCP would have to respond to in order to result in this idealized response. In which case
\begin{align*}
	\pi &= p\sqrt{2/(1-p)}.
\end{align*}

\begin{figure*}
  \centering
  \includegraphics[width=0.7\textwidth]{linux-dctcp-p-unsat-log-lin}\\
  \includegraphics[width=0.7\textwidth]{linux-dctcp-p-unsat-log-log}
\caption{Relationship between the AQM marking probability, \(p\) and the effective marking probability responded to by the Linux implementation of DCTCP, \(\pi=p\sqrt{2/(1-p)}\). The bottom chart shows the same data as the top, but on log-log scales, rather than log-linear. The third plot in each chart is for comparison with \cite{Briscoe17a:CC_Tensions_TR}.}\label{fig:linux-dctcp-p-unsat}
\end{figure*}

The relationship between \(\pi\) and \(p\) is illustrated in \autoref{fig:linux-dctcp-p-unsat}. The approach suggested in \cite{Briscoe17a:CC_Tensions_TR} is also shown for comparison. This approach applies additive increase only on unmarked packets and results in an effective congestion probability of \(p/(1-p)\).  The outcome of the Linux implementation, \(\pi\), has similar properties, but it tends to infinity much more slowly as \(p\) approaches 100\%. In turn, both these approaches are similar to that in \cite{Teymoori19:Additive_path_cost} (not shown), which was designed to ensure the path cost is additive. 

\subsection{Linux DCTCP Congestion Rate}

\begin{figure*}
  \centering
  \includegraphics[width=0.7\textwidth]{linux-dctcp-cong-rate}
\caption{Dependence of the marked packets per round trip of Linux DCTCP, \(\sqrt{2(1-p)}\) on the AQM marking probability, \(p\). The idealized invariant marked packets per round of 2 is also shown as well as a third plot for comparison (see text).}\label{fig:linux-dctcp-cong-rate}
\end{figure*}

A scalable congestion control is defined by a congestion response function that is inversely proportional to the probability of congestion marking, \(p\). This ensures the steady-state number of marked packets per round trip remains constant at any flow rate . In the steady state, the number of marked packets per round trip, denoted by \(v\), is the number of packets per round trip multiplied by the marking probability. That is
\begin{align*}
	v &= pW\\
	  &= \sqrt{2(1-p)}
\end{align*}
This function is illustrated in \autoref{fig:linux-dctcp-cong-rate}, in which it is compared with the idealized number of 2 marked packets per round. Although the Linux Implementation marks fewer packets the more congestion it experiences, as long as congestion is not above 50\%, the number of marked packets is relatively constant (\(1\le v\le\sqrt{2}\)). The condition that congestion is no more than 50\% is equivalent to \(W\ge2\) from \autoref{eqn:cwnd_p}, and it makes sense that, when the window is extremely small, there cannot be more packets marked per round than there are packets per round in the first place.

\autoref{fig:linux-dctcp-cong-rate} also shows a third plot for comparison, which would be the marked packets per round if the non-saturating approach proposed in \cite{Briscoe17a:CC_Tensions_TR} were used. It can be seen that, with rising congestion, the marks per round would tail off more rapidly than the Linux implementation does.

\subsection{Numeric Examples}

A single Linux DCTCP flow with 1500B packets running in a 40Gb/s link over a 10\,ms base round trip with a 500\,\(\mu\)s queue would need to maintain a steady-state congestion window of 40e9*10.5e-3/12e3 = 35,000 segments. Theoretically, by \autoref{eqn:cwnd_p}, this should induce a marking probability of 0.004\% or 1 packet in 25,000. This means that, on average, when CWR state is triggered by a congestion mark, it should last for 35,000 packets. Then on average 24,999 unmarked packets will pass by before the next mark.



\section{Conclusions}

ToDo{}

\subsection{Next Steps}

Take loss response into account.-

Verify model empirically.




